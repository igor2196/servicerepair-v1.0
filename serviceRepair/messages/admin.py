from django.utils.translation import gettext_lazy as _
from django.contrib import admin

from .models import *

@admin.register(Messages)
class MessagesAdmin(admin.ModelAdmin):
	list_display = ('message', 'profile', 'date_time', 'project')
	search_fields = ('message', 'profile__user__username', 'project__name')
	list_filter = ('date_time', 'read_customer','read_performer')