from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
	url(r'^$', views.messages, name='messages'),
	path('dialog/<int:id>/', views.dialog, name='dialog'),
]
