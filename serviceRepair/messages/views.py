from django.shortcuts import render, redirect
from django.urls import reverse
from django.http import Http404
from django.db.models import Q, Count, Max, Case, When
from django.conf import settings
from django.shortcuts import get_object_or_404, get_list_or_404
from django.utils.translation import gettext_lazy as _
from defaultApp.templatetags.auth_extras import has_group
from projects.models import Project
from .models import *
from .forms import *

def messages(request):

	if not request.user.is_authenticated:
		return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

	template = 'profile.html'
	template_inc = 'messages/messages.html'

	userprofile = request.user.userprofile

	if has_group(userprofile.user, "Заказчик"):
		projects = Project.objects.filter(~Q(performer=None), customer=userprofile)

	elif has_group(userprofile.user, "Исполнитель"):
		projects = Project.objects.filter(performer=userprofile)

	result = [];

	for p in projects:

		if has_group(userprofile.user, "Заказчик"):
			count = Messages.objects.filter(~Q(profile=userprofile), project=p, read_customer=False).count()

		elif has_group(userprofile.user, "Исполнитель"):
			count = Messages.objects.filter(~Q(profile=userprofile), project=p, read_performer=False).count()

		if Messages.objects.filter(project=p).count() > 0:
			result.append({
				'project': p,
				'last_message': Messages.objects.filter(project=p).latest('date_time'),
				'count_message': count
			})

	result.sort(key=lambda v: v['last_message'].date_time , reverse=True)

	context = {
		'pageName': _('Сообщения'),
		'activPage': 'Messages',
		'messages': result,
		'template_inc': template_inc,
	}

	return render(request, template, context)

def dialog(request, id):

	if not request.user.is_authenticated:
		return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

	template = 'profile.html'
	template_inc = 'messages/dialog.html'

	userprofile = request.user.userprofile

	if has_group(userprofile.user, "Заказчик"):
		project = get_object_or_404(Project, ~Q(performer=None), customer=userprofile, pk=id)
		profile = project.performer
		Messages.objects.filter(project = project, read_customer=False).exclude(profile = userprofile).update(read_customer=True)

	elif has_group(userprofile.user, "Исполнитель"):
		project =  get_object_or_404(Project, performer=userprofile, pk=id)
		profile = project.customer
		Messages.objects.filter(project = project, read_performer=False).exclude(profile = userprofile).update(read_performer=True)

	if request.method == 'POST':
		messageForm = MessagesForm(request.POST, request.FILES)
		if messageForm.is_valid():
			message_new = messageForm.save(commit=False)
			message_new.project = project
			message_new.profile = request.user.userprofile
			message_new.save()
			messageForm = MessagesForm()
		else:
			messages.error(request, _('Пожалуйста, исправьте ошибки.'))
	else:
		messageForm = MessagesForm()

	messages = get_list_or_404(Messages.objects.filter(project = project).order_by('date_time'))

	context = {
		'pageName': _('Сообщения'),
		'activPage': 'Messages',
		'template_inc': template_inc,
		'messages': messages,
		'profile': profile,
		'project': project,
		'messageForm': messageForm
	}

	return render(request, template, context)