from django.apps import AppConfig


class MessagesConfig(AppConfig):
	name = 'messages'
	label = 'Users_messages'
	verbose_name = 'Сообщения'
