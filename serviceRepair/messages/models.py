from django.db import models
from userProfile.models import UserProfile
from projects.models import Project
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _

# TODO: Прикреплять изображения и файлы
class Messages(models.Model):
	profile = models.ForeignKey(UserProfile, on_delete=models.CASCADE, verbose_name=_('Отправитель'))
	project = models.ForeignKey(Project, on_delete=models.CASCADE, verbose_name=_('Проект'))
	message = models.TextField(verbose_name=_('Сообщение'))
	date_time = models.DateTimeField(auto_now_add=True, verbose_name=_('Дата и время написания сообщения'))
	read_customer = models.BooleanField(default=False, verbose_name=_('Прочитано заказчиком?'))
	read_performer = models.BooleanField(default=False, verbose_name=_('Прочитано исполнителем?'))

	def __str__(self):
		return self.message

	class Meta:
		verbose_name = _('Сообщение')
		verbose_name_plural = _('Сообщения')