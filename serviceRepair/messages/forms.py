from django import forms
from .models import *

class MessagesForm(forms.ModelForm):
	class Meta:
		model = Messages
		fields = ('message',)