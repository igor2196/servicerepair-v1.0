from django import template
from django.db.models import Q
from messages.models import Messages
from defaultApp.templatetags.auth_extras import has_group
from projects.models import Project

register = template.Library()


@register.filter(name='get_message_count')
def get_message_count(userprofile):

	count_message = 0

	if has_group(userprofile.user, "Заказчик"):
		projects = Project.objects.filter(~Q(performer=None), customer=userprofile)

		for p in projects:
			count_message += Messages.objects.filter(~Q(profile=userprofile), project=p, read_customer=False).count()

	elif has_group(userprofile.user, "Исполнитель"):
		projects = Project.objects.filter(performer=userprofile)

		for p in projects:
			count_message += Messages.objects.filter(~Q(profile=userprofile), project=p, read_performer=False).count()

	return count_message