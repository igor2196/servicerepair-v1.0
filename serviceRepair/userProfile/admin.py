from django.utils.translation import gettext_lazy as _
from django.contrib import admin

from .models import *

@admin.register(Towns)
class TownsAdmin(admin.ModelAdmin):
	list_display = ('name',)
	search_fields = ('name',)

@admin.register(Statuses)
class StatusesAdmin(admin.ModelAdmin):
	list_display = ('name',)
	search_fields = ('name',)

@admin.register(SpecializationGroup)
class SpecializationGroupAdmin(admin.ModelAdmin):
	list_display = ('name', 'sort')
	search_fields = ('name',)

@admin.register(Specialization)
class SpecializationAdmin(admin.ModelAdmin):
	list_display = ('name', 'group')
	search_fields = ('name',)
	list_filter = ('group',)

@admin.register(UserProfile)
class UserProfileAdmin(admin.ModelAdmin):
	list_display = ('user', 'get_group')
	search_fields = ('user__username', 'user__groups__name')
	list_filter = ('user__groups',)
	filter_horizontal = ('specializations',)

	def get_group(self, obj):
		return obj.user.groups.first()

	get_group.short_description = _('Тип')
	get_group.admin_order_field = 'user__groups__name'