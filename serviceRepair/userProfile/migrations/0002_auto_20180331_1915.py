# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-03-31 16:15
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userProfile', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userprofile',
            old_name='birth_date',
            new_name='birthDate',
        ),
    ]
