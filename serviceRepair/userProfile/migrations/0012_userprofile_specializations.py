# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-04-08 19:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('userProfile', '0011_auto_20180408_2233'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='specializations',
            field=models.ManyToManyField(to='userProfile.Specialization'),
        ),
    ]
