# Generated by Django 2.0.4 on 2018-04-28 16:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('userProfile', '0016_auto_20180427_1905'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='specialization',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='specialization', to='userProfile.Specialization', verbose_name='Основная специализация'),
        ),
    ]
