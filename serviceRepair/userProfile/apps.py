from django.apps import AppConfig


class UserprofileConfig(AppConfig):
    name = 'userProfile'
    verbose_name = 'Профиль пользователя'