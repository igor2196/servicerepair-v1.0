from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
	url(r'^personal/$', views.personal, name='profilePersonal'),
	url(r'^summary/$', views.summary, name='profileSummary'),
	url(r'^specialization/$', views.specialization, name='profileSpecialization'),
	url(r'^registration/$', views.registration, name='userRegistration'),
	path('<slug:user_name>/', views.profile_public, name='profilePublic'),
]