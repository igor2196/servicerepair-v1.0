from django import template
from userProfile.models import *

register = template.Library()


@register.filter(name='get_group')
def get_group(id):
	spec = Specialization.objects.get(pk=id)
	return spec.group.name