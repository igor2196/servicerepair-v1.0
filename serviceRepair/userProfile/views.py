from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.http import JsonResponse, Http404
from django.conf import settings
from django.contrib import messages
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import Group

from defaultApp.templatetags.auth_extras import has_group
from projects.models import Review
from portfolio.models import Portfolio

from .forms import *
from .models import *


def personal(request):

	if not request.user.is_authenticated:
		return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

	template = 'profile.html'
	template_inc = 'userProfile/personal_form.html'

	if request.method == 'POST':
		userForm = UserForm(request.POST, instance=request.user)
		profileForm = ProfileForm(request.POST, request.FILES, instance=request.user.userprofile)

		if userForm.is_valid() and profileForm.is_valid():
			userForm.save()
			profileForm.save()
			messages.success(request, _('Ваш профиль был успешно обновлен!'))
		else:
			messages.error(request, _('Пожалуйста, исправьте ошибки.'))

		template = template_inc
	else:
		userForm = UserForm(instance=request.user)
		profileForm = ProfileForm(instance=request.user.userprofile)

	context = {
		'pageName': _('Профиль'),
		'activPage': 'Personal',
		'userForm': userForm,
		'profileForm': profileForm,
		'template_inc': template_inc
	}

	if request.method == 'POST':
		result = {
			'html': render_to_string(template, context, request),
			'img': request.user.userprofile.image.url if request.user.userprofile.image else None
		}
		return JsonResponse(result)
	else:
		return render(request, template, context)


def summary(request):

	if not request.user.is_authenticated or not has_group(request.user, "Исполнитель"):
		return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

	template = 'profile.html'
	template_inc = 'userProfile/summary_form.html'

	if request.method == 'POST':
		summaryForm = SummaryForm(request.POST, instance=request.user.userprofile)
		if summaryForm.is_valid():
			summaryForm.save()
			messages.success(request, _('Ваше резюме успешно сохранено!'))
		else:
			messages.error(request, _('Пожалуйста, исправьте ошибки.'))

		template = template_inc
	else:
		summaryForm = SummaryForm(instance=request.user.userprofile)

	context = {
		'pageName': _('Резюме'),
		'activPage': 'Summary',
		'summaryForm': summaryForm,
		'template_inc': template_inc
	}

	return render(request, template, context)

def specialization(request):

	if not request.user.is_authenticated or not has_group(request.user, "Исполнитель"):
		return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

	template = 'profile.html'
	template_inc = 'userProfile/specialization_form.html'

	if request.method == 'POST':
		specializationForm = SpecializationForm(request.POST, instance=request.user.userprofile)
		if specializationForm.is_valid():
			specializationForm.save()
			messages.success(request, _('Основная специализация успешно изменена'))
			messages.success(request, _('Специализации успешно изменены'))
		else:
			messages.error(request, _('Пожалуйста, исправьте ошибки.'))

		template = template_inc
	else:
		specializationForm = SpecializationForm(instance=request.user.userprofile)

	context = {
		'pageName': _('Специализации'),
		'activPage': 'Specialization',
		'specializationForm': specializationForm,
		'template_inc': template_inc
	}

	return render(request, template, context)

def profile_public(request, user_name=None):

	profile = get_object_or_404(UserProfile, user__username=user_name)

	context = {
		'pageName': _('Пользователь'),
		'activPage': 'PublicProfile',
		'profile': profile,
		'reviews': Review.objects.filter(profile_to=profile),
		'portfolio': Portfolio.objects.filter(profile=profile),
		'specializations': profile.specializations.all(),
	}

	return render(request, 'userProfile/profile_public.html', context)

def registration(request):

	active_tab = 'customer'

	if request.method == 'POST':

		global userCreationForm
		global registrationUserForm
		global registrationProfileCustomerForm
		global registrationProfilePerformerForm

		if request.POST.get('type') == 'performer':
			active_tab = 'performer'
			val = registrationPerformer(request)
			if val:
				return val
		elif request.POST.get('type') == 'customer':
			active_tab = 'customer'
			val =  registrationCustomer(request)
			if val:
				return val
	else:
		userCreationForm = UserCreationForm()
		registrationProfileCustomerForm = RegistrationProfilePerformerForm()
		registrationProfilePerformerForm = RegistrationProfilePerformerForm()
		registrationUserForm = RegistrationUserForm()

	context = {
		'pageName': _('Пользователь'),
		'activPage': 'Registration',
		'active_tab': active_tab,
		'userCreationForm': userCreationForm,
		'registrationProfileCustomerForm': registrationProfileCustomerForm,
		'registrationProfilePerformerForm': registrationProfilePerformerForm,
		'registrationUserForm':registrationUserForm,
	}

	return render(request, 'registration/registration.html', context)

def registrationCustomer(request):

	global userCreationForm
	global registrationProfileCustomerForm
	global registrationUserForm
	global registrationProfilePerformerForm
	userCreationForm = UserCreationForm(request.POST)
	registrationProfileCustomerForm = RegistrationProfilePerformerForm(request.POST)
	registrationUserForm = RegistrationUserForm(request.POST)
	registrationProfilePerformerForm = RegistrationProfilePerformerForm()

	if userCreationForm.is_valid() and registrationProfileCustomerForm.is_valid() and registrationUserForm.is_valid():

		user_new = userCreationForm.save(commit=False)
		user_new.last_name = registrationUserForm.cleaned_data['last_name']
		user_new.first_name = registrationUserForm.cleaned_data['first_name']
		user_new.email = registrationUserForm.cleaned_data['email']

		user_new.save()

		group = Group.objects.get(name='Заказчик')
		user_new.groups.add(group)

		user_new.userprofile.town = registrationProfileCustomerForm.cleaned_data['town']
		user_new.userprofile.phone = registrationProfileCustomerForm.cleaned_data['phone']

		user_new.userprofile.save()

		messages.success(request, _('Вы успешно зарегистрированы'))
		return redirect(settings.LOGIN_URL)
	else:
		messages.error(request, _('Пожалуйста, исправьте ошибки.'))
		return None


def registrationPerformer(request):

	global userCreationForm
	global registrationProfileCustomerForm
	global registrationUserForm
	global registrationProfilePerformerForm
	userCreationForm = UserCreationForm(request.POST)
	registrationProfilePerformerForm = RegistrationProfilePerformerForm(request.POST)
	registrationUserForm = RegistrationUserForm(request.POST)
	registrationProfileCustomerForm = RegistrationProfileCustomerForm()

	if userCreationForm.is_valid() and registrationProfilePerformerForm.is_valid() and registrationUserForm.is_valid():

		user_new = userCreationForm.save(commit=False)
		user_new.last_name = registrationUserForm.cleaned_data['last_name']
		user_new.first_name = registrationUserForm.cleaned_data['first_name']
		user_new.email = registrationUserForm.cleaned_data['email']
		user_new.is_active = False
		user_new.save()

		group = Group.objects.get(name='Исполнитель')
		user_new.groups.add(group)

		user_new.userprofile.town = pk=registrationProfilePerformerForm.cleaned_data['town']
		user_new.userprofile.phone = registrationProfilePerformerForm.cleaned_data['phone']
		user_new.userprofile.info = registrationProfilePerformerForm.cleaned_data['info']
		user_new.userprofile.specialization = registrationProfilePerformerForm.cleaned_data['specialization']

		user_new.userprofile.save()

		messages.success(request, _('Ваша заявка на регистрацию успешно отправлена, мы свяжемся с вами'))
		return redirect('home')
	else:
		messages.error(request, _('Пожалуйста, исправьте ошибки.'))
		return None