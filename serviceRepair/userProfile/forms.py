from django import forms
from django.contrib.auth.models import User
from .models import *

class UserForm(forms.ModelForm):
	class Meta:
		model = User
		fields = ('first_name', 'last_name', 'email')


class ProfileForm(forms.ModelForm):
	class Meta:
		model = UserProfile
		fields = ('image', 'birthDate', 'town', 'phone', 'status', 'url')


class SummaryForm(forms.ModelForm):
	class Meta:
		model = UserProfile
		fields = ('summary',)


class SpecializationForm(forms.ModelForm):
	class Meta:
		model = UserProfile
		fields = ('specializations','specialization')
		widgets = {'specializations': forms.CheckboxSelectMultiple()}

class RegistrationProfileCustomerForm(forms.ModelForm):
	class Meta:
		model = UserProfile
		fields = ('phone', 'town')

class RegistrationProfilePerformerForm(forms.ModelForm):
	class Meta:
		model = UserProfile
		fields = ('specialization', 'phone', 'town', 'info')

class RegistrationUserForm(forms.ModelForm):
	class Meta:
		model = User
		fields = ('first_name','last_name', 'email')