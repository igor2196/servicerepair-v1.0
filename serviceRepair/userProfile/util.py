def image_file_name(instance, filename):
	return '/'.join(['users', str(instance.user.id), filename])
