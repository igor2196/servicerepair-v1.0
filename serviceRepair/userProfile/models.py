from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _
from .util import image_file_name


class SpecializationGroup(models.Model):
	name = models.CharField(max_length=250, verbose_name=_('Название'))
	sort = models.PositiveIntegerField(unique = True, verbose_name=_('Сортировка'))

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = _('Группа специализации')
		verbose_name_plural = _('Группы специализаций')

class Specialization(models.Model):
	name = models.CharField(max_length=250, verbose_name=_('Название'))
	group = models.ForeignKey(SpecializationGroup, verbose_name=_('Группа'), on_delete=models.CASCADE)

	def __str__(self):
		return '%s| %s' % (self.group, self.name)

	class Meta:
		ordering = ['group__sort', 'name']
		verbose_name = _('Специализация')
		verbose_name_plural = _('Специализации')

class Towns(models.Model):
	name = models.CharField(max_length=250, verbose_name=_('Название'))

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = _('Город')
		verbose_name_plural = _('Города')


class Statuses(models.Model):
	name = models.CharField(max_length=250, verbose_name=_('Название'))

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = _('Статус')
		verbose_name_plural = _('Статусы')


class UserProfile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE,  verbose_name=_('Пользователь'))
	birthDate = models.DateField(null=True, blank=True, verbose_name=_('Дата рождения'))
	image = models.ImageField(upload_to=image_file_name, null=True, blank=True, verbose_name=_('Изображение профиля'))
	phone = models.CharField(max_length=20, null=True, blank=True, verbose_name=_('Номер телефона'))
	url = models.URLField(max_length=250, null=True, blank=True, verbose_name=_('Веб сайт'))
	town = models.ForeignKey(Towns, on_delete=models.SET_NULL, null=True, blank=True, verbose_name=_('Город'))
	status = models.ForeignKey(Statuses, on_delete=models.SET_DEFAULT, default=1, verbose_name=_('Статус'))
	summary = models.TextField(null=True, blank=True, verbose_name=_('Резюме'))
	skype = models.CharField(max_length=100, null=True, blank=True, verbose_name='Skype')
	specialization = models.ForeignKey(Specialization, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Основная специализация', related_name='specialization')
	specializations = models.ManyToManyField(Specialization, blank=True, verbose_name='Специализации')
	info = models.TextField(null=True, blank=True, verbose_name=_('Информация для проверки'), help_text='Введите информацию которая может подтвердить ваш опыт. Например: контактные данные заказчиков для которых вы выполняли проекты.')

	def __str__(self):
		return self.user.username

	class Meta:
		verbose_name = _('Профиль пользователя')
		verbose_name_plural = _('Профили пользователей')


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
	if created:
		UserProfile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
	instance.userprofile.save()
