from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
	url(r'^$', views.portfolio, name='portfolio'),
	url(r'^all/$', views.portfolio_all, name='portfolioAll'),
	url(r'^add/$', views.portfolio_edit, name='portfolioAdd'),
	path('edit/<int:id>/', views.portfolio_edit, name='portfolioEdit'),
	path('del/<int:id>/', views.portfolio_del, name='portfolioDel'),

	path('<slug:user_name>/<int:id>/',
		views.portfolio_single_public, name='portfolioShow'),
	path('<slug:user_name>/',
		views.portfolio_list_public, name='portfolioListShow'),
]
