from django.contrib import admin

from .models import Portfolio, ImagesPortfolio, FilePortfolio

class ImagesPortfolioInline(admin.TabularInline):
	model = ImagesPortfolio

class FilePortfolioInline(admin.TabularInline):
	model = FilePortfolio

@admin.register(Portfolio)
class PortfolioAdmin(admin.ModelAdmin):
	list_display = ('profile', 'name')
	search_fields = ('name', 'profile__user__username')
	list_filter = ('profile__user__username',)
	inlines = [ImagesPortfolioInline, FilePortfolioInline]