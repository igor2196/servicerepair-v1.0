# Generated by Django 2.0.4 on 2018-04-30 16:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portfolio', '0013_auto_20180419_0055'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fileportfolio',
            name='name',
            field=models.CharField(max_length=30, verbose_name='Название файла'),
        ),
    ]
