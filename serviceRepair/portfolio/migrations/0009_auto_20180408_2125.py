# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-04-08 18:25
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import portfolio.util


class Migration(migrations.Migration):

    dependencies = [
        ('portfolio', '0008_auto_20180408_2113'),
    ]

    operations = [
        migrations.AddField(
            model_name='portfolio',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to=portfolio.util.image_file_name, verbose_name='Главное изображение проекта'),
        ),
        migrations.AlterField(
            model_name='fileportfolio',
            name='portfolio',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='portfolio.Portfolio', verbose_name='Портфолио'),
        ),
        migrations.AlterField(
            model_name='imagesportfolio',
            name='portfolio',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='portfolio.Portfolio', verbose_name='Портфолио'),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='profile',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='userProfile.UserProfile', verbose_name='Профиль'),
        ),
    ]
