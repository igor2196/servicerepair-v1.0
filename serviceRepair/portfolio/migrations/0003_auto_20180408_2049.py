# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-04-08 17:49
from __future__ import unicode_literals

from django.db import migrations, models
import portfolio.util


class Migration(migrations.Migration):

    dependencies = [
        ('portfolio', '0002_auto_20180408_2046'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='imagesportfolio',
            options={'verbose_name': 'Изображение проекта', 'verbose_name_plural': 'Изображения проекта'},
        ),
        migrations.AlterField(
            model_name='imagesportfolio',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to=portfolio.util.image_file_name, verbose_name='Изображение проекта'),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='description',
            field=models.TextField(default='text', verbose_name='Описание'),
            preserve_default=False,
        ),
    ]
