# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-04-18 21:55
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models
import portfolio.util


class Migration(migrations.Migration):

    dependencies = [
        ('portfolio', '0012_auto_20180419_0052'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fileportfolio',
            name='file',
            field=models.FileField(upload_to=portfolio.util.file_name, validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['pdf', 'doc', 'docx', 'xlsx', 'xls', 'zip'])], verbose_name='Файлы проекта'),
        ),
    ]
