from django.shortcuts import render, redirect
from django.urls import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from django.shortcuts import get_object_or_404
from django.contrib import messages
from django.utils.translation import gettext_lazy as _
from defaultApp.templatetags.auth_extras import has_group
from django.http import Http404
from userProfile.models import UserProfile
from .forms import *


def portfolio(request):

	if not request.user.is_authenticated or not has_group(request.user, "Исполнитель"):
		return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

	template = 'profile.html'
	template_inc = 'portfolio/portfolio_list.html'

	portfolio_list = Portfolio.objects.filter(profile=request.user.userprofile).order_by('-id')
	paginator = Paginator(portfolio_list, 5)
	page = request.GET.get('page', 1)

	try:
		portfolios = paginator.page(page)
	except PageNotAnInteger:
		portfolios = paginator.page(1)
	except EmptyPage:
		portfolios = paginator.page(paginator.num_pages)

	context = {
		'pageName': _('Портфолио'),
		'activPage': 'Portfolio',
		'template_inc': template_inc,
		'portfolio_list': portfolios,
		'url_public_portfolio': request.build_absolute_uri(reverse('portfolioListShow', args=(request.user.username, )))
	}

	return render(request, template, context)


def portfolio_edit(request, id=None):

	# TODO: Валидация и вывод ошибок
	# TODO: Валидация размеров файлов и картинок везде!!

	if not request.user.is_authenticated or not has_group(request.user, "Исполнитель"):
		return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

	template = 'profile.html'
	template_inc = 'portfolio/portfolio_form.html'

	if id:
		message_success = _('Проект успешно обновлен.')
		name = _("Редактирование работы")
		portfolio = get_object_or_404(Portfolio, profile=request.user.userprofile, pk=id)
	else:
		message_success = _('Проект успешно добавлен.')
		name = _("Добавление работы")
		portfolio = None

	if request.method == 'POST':
		portfolioForm = PortfolioForm(request.POST, request.FILES, instance=portfolio)
		portfolioImagesFormSet = ImagesInlineFormSet(request.POST, request.FILES, instance=portfolio)
		portfolioFilesFormSet = FilesInlineFormSet(request.POST, request.FILES, instance=portfolio)

		if portfolioForm.is_valid() and portfolioImagesFormSet.is_valid() and portfolioFilesFormSet.is_valid():
			portfolio_new = portfolioForm.save(commit=False)
			portfolio_new.profile = request.user.userprofile
			portfolio_new.save()

			images = portfolioImagesFormSet.save(commit=False)
			files = portfolioFilesFormSet.save(commit=False)

			for obj in portfolioImagesFormSet.deleted_objects:
				obj.delete()
			for image in images:
				image.portfolio = portfolio_new
				image.save()

			for obj in portfolioFilesFormSet.deleted_objects:
				obj.delete()
			for file in files:
				file.portfolio = portfolio_new
				file.save()

			messages.success(request, message_success)
			return redirect('portfolio')
		else:
			messages.error(request, _('Пожалуйста, исправьте ошибки.'))
	else:
		portfolioForm = PortfolioForm(instance=portfolio)
		portfolioImagesFormSet = ImagesInlineFormSet(instance=portfolio)
		portfolioFilesFormSet = FilesInlineFormSet(instance=portfolio)

	context = {
		'pageName': _('Портфолио'),
		'activPage': 'Portfolio',
		'name': name,
		'portfolioForm': portfolioForm,
		'portfolioImagesFormSet': portfolioImagesFormSet,
		'portfolioFilesFormSet': portfolioFilesFormSet,
		'template_inc': template_inc
	}

	return render(request, template, context)


def portfolio_del(request, id):

	if not request.user.is_authenticated or not has_group(request.user, "Исполнитель"):
		return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

	# TODO: Удаление сделать через Post и окно подтверждения перед удалением

	portfolio = get_object_or_404(Portfolio, profile=request.user.userprofile, pk=id)
	portfolio.delete()

	messages.error(request, _('Проект успешно удален.'))

	return redirect('portfolio')


def portfolio_single_public(request, user_name, id):

	portfolio = get_object_or_404(Portfolio, profile__user__username=user_name, pk=id)
	files = FilePortfolio.objects.filter(portfolio = portfolio)
	images =ImagesPortfolio.objects.filter(portfolio = portfolio)

	context = {
		'pageName': _('Портфолио'),
		'activPage': 'portfolio',
		'portfolio': portfolio,
		'files': files,
		'images': images
	}

	# TODO: Доделать верстку публичной страницы проекта

	return render(request, 'portfolio/single_public.html', context)


def portfolio_list_public(request, user_name):

	portfolio_list = Portfolio.objects.filter(profile__user__username=user_name).order_by('-id')

	if not portfolio_list:
		raise Http404

	paginator = Paginator(portfolio_list, 6)
	page = request.GET.get('page', 1)

	try:
		portfolios = paginator.page(page)
	except PageNotAnInteger:
		portfolios = paginator.page(1)
	except EmptyPage:
		portfolios = paginator.page(paginator.num_pages)

	profile = get_object_or_404(UserProfile, user__username=user_name)

	context = {
		'pageName': _('Портфолио'),
		'activPage': 'portfolio',
		'portfolio_list': portfolios,
		'profile': profile
	}

	return render(request, 'portfolio/list_public.html', context)

def portfolio_all(request):

	context = {
		'pageName': _('Работы'),
		'activPage': 'Works',
	}

	return render(request, 'portfolio/list_all.html', context)