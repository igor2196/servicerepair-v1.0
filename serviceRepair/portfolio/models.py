from django.utils.translation import gettext_lazy as _
from django.core.validators import FileExtensionValidator
from django.db import models
from .util import *


class Portfolio(models.Model):
	profile = models.ForeignKey('userProfile.UserProfile', on_delete=models.CASCADE, verbose_name=_('Профиль'))
	image = models.ImageField(upload_to=image_file_name, verbose_name=_('Главное изображение проекта'))
	name = models.CharField(max_length=250, verbose_name=_('Название проекта'))
	description = models.TextField(verbose_name=_('Описание'))

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):
		if self.pk is None:
			saved_image = self.image
			self.image = None
			super(Portfolio, self).save(*args, **kwargs)
			self.image = saved_image

		super(Portfolio, self).save(*args, **kwargs)

	class Meta:
		verbose_name = _('Портфолио')
		verbose_name_plural = _('Портфолио')


class ImagesPortfolio(models.Model):
	portfolio = models.ForeignKey('Portfolio', on_delete=models.CASCADE, verbose_name=_('Портфолио'))
	image = models.ImageField(upload_to=images_file_name, verbose_name=_('Изображение проекта'))

	def __str__(self):
		return 'Изображение'

	class Meta:
		verbose_name = _('Изображение проекта')
		verbose_name_plural = _('Изображения проекта')


class FilePortfolio(models.Model):
	portfolio = models.ForeignKey(Portfolio, on_delete=models.CASCADE, verbose_name=_('Портфолио'))
	name = models.CharField(max_length=30, verbose_name=_('Название файла'))
	file = models.FileField(upload_to=file_name, verbose_name=_('Файлы проекта'), validators=[FileExtensionValidator(allowed_extensions=['pdf', 'doc', 'docx', 'xlsx', 'xls', 'zip'])])

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = _('Файл проекта')
		verbose_name_plural = _('Файлы проекта')