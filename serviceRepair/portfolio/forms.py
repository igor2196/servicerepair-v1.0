from django import forms
from .models import *

class PortfolioForm(forms.ModelForm):
	class Meta:
		model = Portfolio
		fields = ('name', 'image', 'description')

ImagesInlineFormSet = forms.inlineformset_factory(
	Portfolio,
	ImagesPortfolio,
	fields = '__all__',
	max_num = 3
)

FilesInlineFormSet = forms.inlineformset_factory(
	Portfolio,
	FilePortfolio,
	fields = '__all__',
	max_num = 2
)