def image_file_name(instance, filename):
	return '/'.join(['users', str(instance.profile.user.id), 'portfolio_' + str(instance.id), 'images', filename])

def images_file_name(instance, filename):
	return '/'.join(['users', str(instance.portfolio.profile.user.id), 'portfolio_' + str(instance.portfolio.id), 'images', filename])

def file_name(instance, filename):
	return '/'.join(['users', str(instance.portfolio.profile.user.id), 'portfolio_'+ str(instance.portfolio.id), 'files', filename])