from django.utils.translation import gettext_lazy as _
from django.apps import AppConfig


class PortfolioConfig(AppConfig):
	name = 'portfolio'
	verbose_name = _('Портфолио')
