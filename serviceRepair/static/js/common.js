$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip({
		animated: 'fade',
		html: true
	});

	window.setTimeout(function() {
		$(".alert-slide-up").slideUp("slow", function(){
			$(this).remove();
		});
	}, 3000);

	$(".select-2").select2({
		language: "ru"
	});

	$('.form-include').on('submit', 'form.ajax', function(e) {
		e.preventDefault();
		$('.loader-container').addClass('flex');
		var form = $(this),
			formData = new FormData(this);
		$.ajax({
			type: $(form).attr('method') || "POST",
			url: $(form).attr('action') || location.href,
			contentType: false,
			processData: false,
			data: formData,
			success: function(data){
				$('.form-include').html(data);
				$("select").select2();
				$('.loader-container').removeClass('flex');
				destroyAlert('.alert-success', 1500);
				destroyAlert('.alert-danger', 3000);
			},
			error: function(xhr, ajaxOptions, thrownError) {
				$('.loader-container').removeClass('flex');
				alert(thrownError + '\n' + xhr.status + '\n' + ajaxOptions);
			}
		});
	});

	$('.btn-select-performer').click(function(event) {
		$('#id_performer').val($(this).data('performer'));
	});

	$('.btn-project-extend').click(function(event) {
		$('#projectExtend').val($(this).data('project'));
	});

	$('.btn-project-end').click(function(event) {
		$('#projectEnd').val($(this).data('project'));
	});

	$('.btn-project-remove').click(function(event) {
		$('#projectRemove').val($(this).data('project'));
	});

	$('.btn-project-close').click(function(event) {
		$('#projectClose').val($(this).data('project'));
	});

	$('.form-include').on('submit', 'form.ajaxPersonal', function(e) {
		e.preventDefault();
		$('.loader-container').addClass('flex');
		var form = $(this),
			formData = new FormData(this);
		$.ajax({
			type: $(form).attr('method') || "POST",
			url: $(form).attr('action') || location.href,
			contentType: false,
			processData: false,
			data: formData,
			success: function(data){
				$('.form-include').html(data.html);
				if (data.img){
					$('.profile-image.hidden-xs').replaceWith('<img src="'+data.img+'" class="profile-image hidden-xs" alt="Изображение профиля">');
					$('.profile-image:not(.hidden-xs)').replaceWith('<img src="'+data.img+'" class="profile-image" alt="Изображение профиля">');
				}
				else{
					$('.profile-image.hidden-xs').replaceWith('<div class="profile-image hidden-xs"><i class="fa fa-user-secret" aria-hidden="true"></i></div>');
					$('.profile-image:not(.hidden-xs)').replaceWith('<div class="profile-image"><i class="fa fa-user-secret" aria-hidden="true"></i></div>');
				}

				$("select").select2();
				$('.loader-container').removeClass('flex');
				destroyAlert('.alert-success', 1500);
				destroyAlert('.alert-danger', 3000);
			},
			error: function(xhr, ajaxOptions, thrownError) {
				$('.loader-container').removeClass('flex');
				alert(thrownError + '\n' + xhr.status + '\n' + ajaxOptions);
			}
		});
	});

	if(typeof ClipboardJS == 'function') {
		var clipboard = new ClipboardJS('.btn-copy');

		clipboard.on('success', function(e) {
			$(".alert-copy").show();
			setTimeout(function() { $(".alert-copy").fadeOut("slow"); }, 1000);
		});
	}

	$('.scroll').each(function(index, el) {
		window.scrollElement = new SimpleBar( el, { autoHide: false });
	});

	if($(".messages-list").length > 0){
		window.scrollElement.getScrollElement().scrollTop= $(".messages-list")[0].scrollHeight;
	}

	$('input[type="tel"]').mask("+7 (999)-99-99-999");
	$('input.date-input').mask("99.99.9999");
});

function destroyAlert(name, time){
	var timerId = setInterval(function() {
		var arr = $(name);

		if($(arr).length == 0)
			clearInterval(timerId);

		$(arr[0]).fadeOut( "slow", function(){ $(this).remove(); });
	}, time);
}