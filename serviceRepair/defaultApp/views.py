from django.shortcuts import render, redirect
from django.conf import settings
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _


def index(request):

	context = {
		'pageName': _('Главная'),
	}

	return render(request, 'defaultApp/index.html', context)

def listСustomers(request):

	users = User.objects.filter(groups__name='Заказчик').order_by('last_name')

	# TODO: Количестово активных проектов
	# TODO: Количестово закрытых проектов
	# TODO: Общее количество проектов
	# TODO: Рейтинг
	# TODO: Количество положительных отзывов
	# TODO: Количество отрицательных отзывов

	context = {
		'activPage': 'Customers',
		'pageName': _('Список заказчиков'),
		'users': users
	}

	return render(request, 'defaultApp/listСustomers.html', context)

def listPerformers(request):

	users = User.objects.filter(groups__name='Исполнитель').order_by('last_name')

	context = {
		'activPage': 'Performers',
		'pageName': _('Список исполнителей'),
		'users': users
	}

	return render(request, 'defaultApp/listPerformers.html', context)

def userSettings(request):

	if not request.user.is_authenticated:
		return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

	template = 'profile.html'
	template_inc = 'defaultApp/userSettings.html'

	context = {
		'activPage': 'UserSettings',
		'template_inc': template_inc,
		'pageName': _('Настройки'),
	}

	return render(request, template, context)