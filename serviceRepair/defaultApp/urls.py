from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^$', views.index, name='home'),
	url(r'^customers/$', views.listСustomers, name='listСustomers'),
	url(r'^performers/$', views.listPerformers, name='listPerformers'),
	url(r'^settings/$', views.userSettings, name='userSettings'),
]