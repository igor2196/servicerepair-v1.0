from django import template

register = template.Library()

@register.simple_tag()
def vardump(var):
	return vars(var)

@register.simple_tag()
def dirdump(var):
	return dir(var)