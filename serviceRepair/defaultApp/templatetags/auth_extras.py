from django import template
from django.contrib.auth.models import Group
from django.conf import settings

register = template.Library()


@register.filter(name='has_group')
def has_group(user, group_name):
	group = Group.objects.get(name=group_name)
	return True if group in user.groups.all() else False

@register.filter(name='is_file_type')
def is_file_type(path, extension):
	return path[-len(extension):] == extension

@register.filter(name='get_price')
def get_price(price):
	return price - ((price*settings.PERCENT)/100)