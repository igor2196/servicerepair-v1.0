from django import template

register = template.Library()

@register.simple_tag
def define(val=None):
	return val

@register.filter(name='get_parameter')
def get_parameter(list):

	result = ''

	for name in list:
		if name[0] !='page':
			for value in name[1]:
				result += '&'+name[0]+'='+value

	return result