from django import template

register = template.Library()

@register.filter(name='string_after')
def string_after(string, char):
	return string.split(char)[1].strip()

@register.filter(name='string_before')
def string_before(string, char):
	return string.split(char)[0].strip()