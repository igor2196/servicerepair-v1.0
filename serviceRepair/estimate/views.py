from django.shortcuts import render, redirect
from django.conf import settings
from defaultApp.templatetags.auth_extras import has_group
from django.utils.translation import gettext_lazy as _

from .models import TypeDesign


def index(request):

	context = {
		'isEstimatePage': True,
		'pageName': _('Расчет сметы'),
		'typeDesigns': TypeDesign.objects.all()
	}

	return render(request, 'estimate/index.html', context)
