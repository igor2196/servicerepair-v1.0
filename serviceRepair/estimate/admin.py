from django.contrib import admin

from .models import TypeDesign

admin.site.register(TypeDesign)