$( document ).ready(function() {

	window.configPa = {}; // создаем пустой конфиг дизайна
	window.configPa.default = {}; // создаем пустые пареметры по умолчанию в конфиге дизайна
	window.configPa.scenes = {}; // создаем пустые сцены в конфиге дизайна
	window.first = true; // признак того что цикл пошел только один раз
	$('.render').each(function(index, el) {
		if(first){ // если это первый раз
			window.configPa.default = { // заполняем конфиг поумолчанию
		        "firstScene": $(el).data('sceneid'), // ставим первую сцену сценой по умолчанию
		        "mouseZoom": false,
		        "autoLoad": true, // автозагрузка сцен
		        "autoRotate": -5, // вращение
		    };

		    window.first = false; // сбрасываем признак первого раза
		}
		window.configPa.scenes[$(el).data('sceneid')] = { // создаем сцену
            "title": $(el).data('titlescene'), // текст к сцене
            "hfov": 110,
            "pitch": 0,
            "yaw": 0,
            "type": "equirectangular", // тип сцены
            "panorama": $(el).data('image'), // картинка для сцены
            "preview": $(el).css('background-image').replace('url(','').replace(')','').replace(/\"/gi, ""),
        }
	});

	delete window.first; // удалить признак

	// обработчик события отображения вкладки дизайн
	$('#designTab').on('shown.bs.tab', renderActivScen);

	/// обработчик события клика на дизайны
	$('#design').on('click','.render', function(event) {
		event.preventDefault();

		var textactive = $(this).text().trim();

		window.pannel.loadScene($(this).data('sceneid')); // загрузить сцену по id

		$('.render').removeClass('activDesign'); // сбросить активный дизайн

		// исправление бага двойной надписи на дизайне
		$('.pnlm-panorama-info').each(function(index, el) {
			if($(el).children('.pnlm-title-box').text() != textactive)
				$(el).css('display', 'none');
		});

		$("[data-sceneid='"+$(this).data('sceneid')+"']").addClass('activDesign'); // установить активный дизайн текущим

		//$(this).addClass('activDesign'); // установить активный дизайн текущим

	});

	function renderActivScen(event){
		if(event)
			event.preventDefault();

		window.configPa.default.firstScene = $('.activDesign').data('sceneid'); // изменить сцену по умолчанию на текущую

		window.pannel = pannellum.viewer('panorama', window.configPa); // перериовать панель
		// нужно что бы избежать черго экрана при загрузке
	}

	renderActivScen();

	$('.owl-carousel').owlCarousel({
	    loop:true,
	    margin:10,
	    nav:true,
	    mouseDrag: false,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:3
	        },
	        1000:{
	            items:4
	        }
	    }
	})

	Constructors.init({
        count: 1,
        selector: "#constructor",
        width: 1000,
        height: 480,
        room: {
            rightAngles: true
        }
    });
});

// garvild-1996@mail.ru