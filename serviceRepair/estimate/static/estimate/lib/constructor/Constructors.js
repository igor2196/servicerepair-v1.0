function Constructors(){

}

Constructors.init = function(opt){

    this.selector = opt.selector;
    this.constructorArr = [];

    opt.wallImg = this.wallImg;

    var html = '<div class="constructors">';

    for(var i = 0; i < opt.count; i++){

        html += '<div class="constructor"><canvas id="constructorCanvas' + i + '" width="' + opt.width + '" height="' + opt.height + '" style="border: 1px solid black" data-width="' + opt.width + '" data-height="' + opt.height + '"></canvas></div>';

        opt.selector = "#constructorCanvas" + i;
    }

    $(this.selector).replaceWith(html);

    for(var i = 0; i < opt.count; i++){

        this.constructorArr[i] = new Constructor(opt);
    }
}