window.wallImg = new Image();
window.wallImg.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAoUlEQVRIx+2WsY5DMQgE/fOYwh9rr6hhUsXKSVflWalcjkCrFdPQqorMZM6JmRERAI9ZEmMMWmYiid47kqgq1lqP2d2RRJtz0ntnzrmbnOC1FgDNzJC0h+7+Z/lbrioighYRVNXR8LcDM6MB+4anwj/P9q+DE+HXwXVwHVwH18FPHJziJgl3Z61FVe0H4ClLIjNpY4ztICIws93kCWcmVcUL8Iz35udy/loAAAAASUVORK5CYII=';

function Wall(parent, startPoint, finishPoint){

    var wall = this;

    this.parent = parent;

    this.move = false;
    this.con  = parent.con;
    this.ctx = this.con.getContext("2d");
    this.startPoint = startPoint;
    this.finishPoint = finishPoint;

    this.length = 0;

    this.color = this.ctx.createPattern(window.wallImg, 'repeat');
    this.firstStateX = 0;
    this.firstStateY = 0;

    this.draw = function() {

        var thickness = this.startPoint.thickness,
            startX = this.startPoint.x + thickness/2,
            finishX = this.finishPoint.x + thickness/2,
            startY = this.startPoint.y + thickness/2,
            finishY = this.finishPoint.y + thickness/2;

        this.length = lengthPoint(this.startPoint, this.finishPoint, this.parent.scaleArea);

        this.ctx.beginPath();
        this.ctx.strokeStyle = "black";
        this.ctx.lineWidth = thickness;
        this.ctx.lineCap = "square";
        this.ctx.moveTo(startX, startY);
        this.ctx.lineTo(finishX, finishY);
        this.ctx.stroke();
        this.ctx.closePath();

        this.ctx.beginPath();
        this.ctx.strokeStyle = this.color;
        this.ctx.fillStyle = this.color;
        this.ctx.lineWidth = thickness - 4;
        this.ctx.lineCap = "square";
        this.ctx.moveTo(startX, startY);
        this.ctx.lineTo(finishX, finishY);
        
        this.ctx.fill();
        this.ctx.stroke();
        this.ctx.closePath();

        drawLabel(this.ctx, (this.length).toFixed(2), startPoint, finishPoint, thickness);

        this.startPoint.draw();
        this.finishPoint.draw();
    };

    $(this.con).on('mousedown',function (e) {
        var x = e.offsetX, y = e.offsetY,
            flag = checkPointInLine(wall.startPoint.x, wall.startPoint.y, 
                wall.finishPoint.x, wall.finishPoint.y, wall.startPoint.thickness, x, y);

        if(flag)
        {
            if(!checkPointInRect(x, y, wall.startPoint.x, wall.startPoint.y, wall.startPoint.thickness) && !checkPointInRect(x, y, wall.finishPoint.x, wall.finishPoint.y, wall.startPoint.thickness)){
                
                wall.move = true;
                wall.firstStateX = x;
                wall.firstStateY = y;
                wall.color = "red";
            }
        }
    });

    $(this.con).on('mouseup', function () {
        wall.move = false;
        wall.firstStateX = 0;
        wall.firstStateY = 0;
        wall.color = wall.ctx.createPattern(window.wallImg, 'repeat');
    });

    $(this.con).on('mousemove', function (e) {
        var x = e.offsetX, y = e.offsetY;

        if(wall.move){
            if(wall.parent.rightAngles){

                switch(wall.startPoint.name+wall.finishPoint.name) {
                    case 'AB':
                    case 'CD':  
                        wall.startPoint.y += y - wall.firstStateY;
                        wall.finishPoint.y += y - wall.firstStateY;
                    break
                    case 'BC':  
                    case 'DA':
                        wall.startPoint.x += x - wall.firstStateX;
                        wall.finishPoint.x += x - wall.firstStateX;
                    break
                }

                wall.firstStateX = x;
                wall.firstStateY = y;

                return;
            }
            wall.startPoint.x += x - wall.firstStateX;
            wall.finishPoint.x += x - wall.firstStateX;

            wall.startPoint.y += y - wall.firstStateY;
            wall.finishPoint.y += y - wall.firstStateY;

            wall.firstStateX = x;
            wall.firstStateY = y;
        }
    });
}