function Room(options) {

    /// текущая комната
    var room = this;

    /// зажата ли кнопка мыши над комнатой
    this.move = false;

    /// прямые углы или нет
    this.rightAngles = options.rightAngles;

    /// канвас и контекст
    this.con  = options.conv;
    this.ctx = this.con.getContext("2d");

    /// ширина квадрата
    this.width = options.width;

    /// высота квадрата   
    this.height = options.height;

    /// толщина рамки
    this.thickness = 14;

    /// кординаты левого верхнего угла
    this.x = options.x; 
    this.y = options.y;

    this.scaleArea = options.scaleArea;

    this.selectAngle = null;

    /// площадь пола
    this.floorArea = 0;

    /// начальные позиции мыши при изменении размера
    this.firstStateX = 0;
    this.firstStateY = 0;

    /// углы
    var leftTopAngle = new Angle(this, this.x, this.y, this.thickness, "A"),
        rightTopAngle = new Angle(this, this.x + this.width, this.y, this.thickness, "B"), 
        rightBottomAngle = new Angle(this, this.x + this.width , this.y + this.height, this.thickness, "C"),
        leftBottomAngle = new Angle(this, this.x, this.y + this.height , this.thickness, "D");

    /// точки по умолчанию
    this.points = [leftTopAngle, rightTopAngle, rightBottomAngle, leftBottomAngle];

    /// стены по умолчанию
    this.walls = [ 
        new Wall(this, leftTopAngle, rightTopAngle), 
        new Wall(this, rightTopAngle, rightBottomAngle), 
        new Wall(this, rightBottomAngle, leftBottomAngle), 
        new Wall(this, leftBottomAngle, leftTopAngle) 
    ];

    /// рисуем комнату
    this.draw = function(){

        /// начинаем рисовать задний фон комнаты
        this.ctx.beginPath(); 

        /// проходим по точкам и рисуем прямые
        for (var i = 0; i < this.points.length; i++) { 

            if (i == 0) this.ctx.moveTo(this.points[i].x + this.thickness/2, 
                this.points[i].y + this.thickness/2); 
            
            else this.ctx.lineTo(this.points[i].x + this.thickness/2, 
                this.points[i].y + this.thickness/2); 
        }

        this.ctx.fillStyle = "white";         
        this.ctx.fill();
        this.ctx.closePath();
        /// заканчиваем рисовать задний фон комнаты
        
        /// вычисляем площадь
        this.floorArea = area(this.points, this.scaleArea);

        // рисуем стены
        $(this.walls).each(function(index, el) {
            el.draw();
        });

        var txt = "S = "+ (this.floorArea).toFixed(2) +" м" + String.fromCharCode(178);

        /// задаем шрифт
        this.ctx.font = "13px 'Comic Sans MS'";
        
        /// достаем ширину текста
        var width = this.ctx.measureText(txt).width;

        /// рисуем задний фон текста
        this.ctx.fillRect(this.points[0].x + this.thickness*3-5, 
            this.points[0].y + this.thickness*2-5, width+10, parseInt(this.ctx.font, 15)+10);
        
        /// меняем цвет залчивки
        this.ctx.fillStyle = "black";

        /// рисуем текст
        this.ctx.fillText(txt, this.points[0].x + this.thickness*3, 
            this.points[0].y + this.thickness*3);
    };

    $(this.con).on('mousedown',function (e) {
        var x = e.offsetX, y = e.offsetY;
        room.move = true;
        room.firstStateX = x;
        room.firstStateY = y;
    });

    $(this.con).on('mouseup', function () {
        room.move = false;
        room.draw();
    });

    $(this.con).on('mousemove', function (e) {
        var x = e.offsetX, y = e.offsetY;

        if(room.move){
            if(room.rightAngles && room.selectAngle){
                switch(room.selectAngle.name) {
                    case 'A':

                        room.walls[0].startPoint.y += y - room.firstStateY;
                        room.walls[0].finishPoint.y += y - room.firstStateY;

                        room.walls[3].startPoint.x += x - room.firstStateX;
                        room.walls[3].finishPoint.x += x - room.firstStateX;

                    break
                    case 'B':  
                        
                        room.walls[0].startPoint.y += y - room.firstStateY;
                        room.walls[0].finishPoint.y += y - room.firstStateY;

                        room.walls[1].startPoint.x += x - room.firstStateX;
                        room.walls[1].finishPoint.x += x - room.firstStateX;

                    break
                    case 'C':

                        room.walls[1].startPoint.x += x - room.firstStateX;
                        room.walls[1].finishPoint.x += x - room.firstStateX;

                        room.walls[2].startPoint.y += y - room.firstStateY;
                        room.walls[2].finishPoint.y += y - room.firstStateY;

                    break
                    case 'D':

                        room.walls[2].startPoint.y += y - room.firstStateY;
                        room.walls[2].finishPoint.y += y - room.firstStateY;

                        room.walls[3].startPoint.x += x - room.firstStateX;
                        room.walls[3].finishPoint.x += x - room.firstStateX;
                    break
                }   
            }

            room.ctx.clearRect(0, 0, $(this).width(), $(this).height());
            
            // room.ctx.translate(x - room.firstStateX, y - room.firstStateY);
            drawGrid(room.con, room.ctx, x, y);
            room.draw();
            room.firstStateX = x;
            room.firstStateY = y;
        }
    });
}