/**
 * Проверяет попадает ли точка в квадрат
 * @param  {float}    x     координата x
 * @param  {float}    y     координата y
 * @param  {float}    px    координата x левого верхнего угла
 * @param  {float}    py    координата y левого верхнего угла
 * @param  {float}    width ширина квадрата
 * @return {boolean}        результат проверки
 */
function checkPointInRect(x, y, px, py, width) {
    return x > px && x < (px + width) && y > py && y < (py + width)
}

/**
 * Вычисление площади многоугольника
 * @param  {Angle} points Углы многоугольника
 * @return {float}        Площадь
 */
function area(points, s){

    var tmpPlus = 0,
        tmpMinus = 0,
        scale = s || 1;

    /// ссылка на формулу
    /// https://ru.wikipedia.org/wiki/Формула_площади_Гаусса

    for (var i = 0, j = points.length - 1; i < points.length; i++, j--) { 
        if(i == points.length - 1) tmpPlus += (points[i].x/scale) * (points[0].y/scale);
        else tmpPlus +=( points[i].x/scale) * (points[i+1].y/scale);

        if(j == points.length - 1) tmpMinus += (points[0].x/scale) * (points[j].y/scale);
        else tmpMinus += (points[j+1].x/scale) * (points[j].y/scale);
    }

    return (1/2)*Math.abs(tmpPlus - tmpMinus);

}

/**
 * Проверяет попадает ли точка на отрезок
 * @param  {float}   startX    координата x начала отрезка
 * @param  {float}   startY    координата y начала отрезка
 * @param  {float}   finishX   координата x конца отрезка
 * @param  {float}   finishY   координата y конца отрезка
 * @param  {int}     thickness ширина прямой
 * @param  {float}   x         координата x точки
 * @param  {float}   y         координата y точки
 * @return {boolean}           результат проверки
 */
function checkPointInLine(startX, startY, finishX, 
    finishY, thickness, x, y) {

    var startX = startX + thickness/2,
        finishX = finishX + thickness/2,
        startY = startY + thickness/2,
        finishY = finishY + thickness/2,
        dx1 = finishX - startX,
        dy1 = finishY - startY,
        dx = x - startX,
        dy = y - startY,
        S = dx1 * dy - dx * dy1,
        ab = Math.sqrt(dx1 * dx1 + dy1 * dy1),
        h = S / ab,
        mp1X = startX - x,
        mp1Y = startY - y,
        mp2X = finishX - x,
        mp2Y = finishY - y,
        flag = mp1X*mp2X+mp1Y*mp2Y;

        if (Math.abs(h) < thickness/2 && flag<=0)
            return true;
        return false;
}

function lengthPoint(startPoint, finishPoint, s){

    var scale = s || 1;

    return Math.sqrt(Math.pow(finishPoint.x/scale - startPoint.x/scale, 2) + 
                Math.pow(finishPoint.y/scale - startPoint.y/scale, 2))
}

function drawLabel( ctx, text, p1, p2, thickness){
    
    // todo: функция пишет текст около прямой
    var dx = (p2.x + thickness/2) - (p1.x + thickness/2);
    var dy = (p2.y + thickness/2) - (p1.y + thickness/2);   
    var p, pad;

    p = p1;
    pad = 1/2;

    ctx.save();
    ctx.font = "11px Arial";
    ctx.fillStyle = "black";
    ctx.textAlign = 'center';
    ctx.translate(p.x+dx*pad,p.y+dy*pad);
    // todo: разобраться с углами
    if(dx < 0){
        ctx.rotate(Math.atan2(dy,dx) - Math.PI);  //to avoid label upside down
        if(dx < -180)
            ctx.fillText(text,0,10);
        else
            ctx.fillText(text,0,10);
    }
    else{
        ctx.rotate(Math.atan2(dy,dx));
        if(dy < 180)
            ctx.fillText(text,0,10);
        else
            ctx.fillText(text,0,10);
    }

    
    ctx.restore();
}

/// Рисуем сетку
function drawGrid(con, ctx) {

    var width = $(con).data("width"),
        height = $(con).data("height");

    ctx.beginPath();

    ctx.lineWidth = 1;

    for (var x = 0.5; x < width; x += 5) {
        ctx.moveTo(x, 0);
        ctx.lineTo(x, height);
    }

    for (var y = 0.5; y < height; y += 5) {
        ctx.moveTo(0, y);
        ctx.lineTo(width, y);
    }

    ctx.strokeStyle = "#E2E2E2";
    ctx.stroke();
    ctx.closePath();

    ctx.beginPath();

    for (var x = 0.5; x < width; x += 50) {
        ctx.moveTo(x, 0);
        ctx.lineTo(x, height);
    }

    for (var y = 0.5; y < height; y += 50) {
        ctx.moveTo(0, y);
        ctx.lineTo(width, y);
    }

    ctx.strokeStyle = "#BBBBBB";
    ctx.stroke();
    ctx.closePath();
}