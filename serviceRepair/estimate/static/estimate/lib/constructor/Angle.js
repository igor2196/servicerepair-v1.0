/**
 * Класс угла
 * @param {[type]} conv       Полотно
 * @param {[type]} x          Кордина x левого верхнего угла
 * @param {[type]} y          Кордина y левого верхнего угла
 * @param {[type]} thickness  Ширина и высота
 * @param {[type]} name       Название
 */
function Angle(parent, x, y, thickness, name) {

    var angle = this;

    this.parent = parent;
    this.con  = parent.con;
    this.ctx = this.con.getContext("2d");

    this.thickness = thickness;

    this.move = false;

    this.x = x;
    this.y = y;
    this.bgcolor = "black";
    this.name = name;

    this.draw = function(){

        this.ctx.beginPath();
        this.ctx.rect(this.x, this.y, this.thickness, this.thickness);
        this.ctx.fillStyle = this.bgcolor;
        this.ctx.fill();
        this.ctx.closePath();

        this.ctx.beginPath();
        this.ctx.font = "11px Arial";
        this.ctx.fillStyle = "white";
        this.ctx.fillText(this.name, this.x + this.thickness/4, 
            this.y + this.thickness - this.thickness/4);
        this.ctx.closePath();
    };

    $(this.con).on('mousedown',function (e) {
        var x = e.offsetX, y = e.offsetY;

        if(checkPointInRect(x, y, angle.x, angle.y, angle.thickness)){
            angle.move = true;
            angle.bgcolor = "red";
            angle.parent.selectAngle = angle;
        }
    });

    $(this.con).on('mouseup', function () {
        angle.move = false;
        angle.bgcolor = "black";
        angle.parent.selectAngle = null;
    });

    $(this.con).on('mousemove', function (e) {
        var x = e.offsetX, y = e.offsetY;
        if(!angle.parent.rightAngles && angle.move){
            angle.x = x;
            angle.y = y;
        }
    });
}