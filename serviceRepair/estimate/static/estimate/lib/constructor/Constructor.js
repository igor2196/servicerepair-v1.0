function Constructor(opt) {

    this.con = $(opt.selector)[0];

    this.width = opt.width;
    this.height = opt.height;

    this.options = {
        selector: opt.selector,

        conv: this.con,

        rightAngles: (opt.room && opt.room.rightAngles != undefined) ? opt.room.rightAngles : true,

        width: (opt.room && opt.room.width) ? opt.room.width : 180,

        height: (opt.room && opt.room.height) ? opt.room.width : 180,

        x: (opt.room && opt.room.x) ? opt.room.x : 
            this.width/2 - ((opt.room && opt.room.width/2) ? opt.room.width/2 : 90),

        y: (opt.room && opt.room.y) ? opt.room.y : 
            this.height/2 - ((opt.room && opt.room.height/2) ? opt.room.height/2 : 90),

        scaleArea: (opt.room && opt.room.scaleArea) ? opt.room.scaleArea : 50,

        wallImg: opt.wallImg
    };

    this.room = new Room(this.options);

    drawGrid(this.con, this.con.getContext("2d"));

    this.room.draw();

    this.ctx = this.con.getContext("2d");
}