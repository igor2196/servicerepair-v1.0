from django.utils.translation import gettext_lazy as _
from django.db import models


class TypeDesign(models.Model):
    name = models.CharField(max_length=200, verbose_name=_('Название'))
    image = models.ImageField(upload_to='design/', verbose_name=_('Дизайн'))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Типовой дизайн')
        verbose_name_plural = _('Типовые дизайны')