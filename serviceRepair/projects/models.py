from django.db import models
from django.utils.translation import string_concat
from django.conf import settings
from django.utils import timezone
from django.urls import reverse_lazy
from django.core.validators import FileExtensionValidator, MaxValueValidator
from django.utils.translation import gettext_lazy as _
from userProfile.models import UserProfile, Specialization, Towns
from .util import *

class ProjectStatus(models.Model):
	name = models.CharField(max_length=100, verbose_name=_('Название'))

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = _('Статус')
		verbose_name_plural = _('Статусы')


class Project(models.Model):
	customer = models.ForeignKey(UserProfile, on_delete=models.CASCADE, verbose_name=_('Заказчик'), related_name='customer')
	name = models.CharField(max_length=150, verbose_name=_('Название'))
	description = models.TextField(verbose_name=_('Описание'), help_text="Описание должно быть достаточно подробным, чтобы заинтересовать заказчиков и дать им возможность оценить объем и стоимость работы. Внимание: быстрый зароботок, реклама сайтов, дубликаты и явно неподходящие по тематике проекты будут беспощадно удаляться!")
	status = models.ForeignKey(ProjectStatus, on_delete=models.CASCADE, default = 9, verbose_name=_('Статус'))
	town = models.ForeignKey(Towns, on_delete=models.CASCADE, verbose_name=_('Город'))
	specialization = models.ForeignKey(Specialization, on_delete=models.CASCADE, verbose_name=_('Категория'))
	money = models.DecimalField(verbose_name=_('Бюджет'), max_digits=19, decimal_places=2, help_text="Комиссия сайта {}%".format(settings.PERCENT))
	term = models.PositiveIntegerField(verbose_name=_('Желаемый срок выполнения'), help_text='Укажите срок в днях')
	relevant_to = models.DateField(verbose_name=_('Актуален до'))
	date_time = models.DateTimeField(default=timezone.now, verbose_name=_('Дата и время создания'))
	estimate = models.FileField(upload_to=estimate_name, null=True, blank=True, verbose_name=_('Смета'),
		validators=[FileExtensionValidator(allowed_extensions=['pdf', 'doc', 'docx', 'xlsx', 'xls'])],
		help_text=string_concat(
			u'Укажите файл сметы либо воспользуйтесь <a target="_blank" href="',
			reverse_lazy("estimate"),
			u'">конструтором</a> для расчета'
			)
		)
	performer = models.ForeignKey(UserProfile, on_delete=models.SET_NULL, null=True, blank=True, verbose_name=_('Исполнитель'), related_name='performer')
	manager = models.ForeignKey(UserProfile, on_delete=models.SET_NULL, null=True, blank=True, verbose_name=_('Менеджер проекта'), related_name='manager')

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = _('Проект')
		verbose_name_plural = _('Проекты')


class FileProject(models.Model):
	project = models.ForeignKey(Project, on_delete=models.CASCADE, verbose_name=_('Проект'))
	name = models.CharField(max_length=30, verbose_name=_('Название файла'))
	file = models.FileField(upload_to=file_name, verbose_name=_('Файлы проекта'), validators=[FileExtensionValidator(allowed_extensions=['pdf', 'doc', 'docx', 'xlsx', 'xls', 'zip'])])

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = _('Файл проекта')
		verbose_name_plural = _('Файлы проекта')


class Rate(models.Model):
	project = models.ForeignKey(Project, on_delete=models.CASCADE, verbose_name=_('Проект'))
	performer = models.ForeignKey(UserProfile, on_delete=models.CASCADE, verbose_name=_('Исполнитель'))
	message = models.TextField(verbose_name=_('Сообщение'))
	term = models.PositiveIntegerField(verbose_name=_('Срок'), help_text='Укажите срок в днях')
	money = models.DecimalField(verbose_name=_('Бюджет'), max_digits=19, decimal_places=2)
	date_time = models.DateTimeField(auto_now_add=True, verbose_name=_('Дата и время добавления ставки'))

	def __str__(self):
		return self.message

	class Meta:
		unique_together = ['project', 'performer']
		verbose_name = _('Ставка')
		verbose_name_plural = _('Ставки')

class Review(models.Model):
	project = models.ForeignKey(Project, on_delete=models.CASCADE, verbose_name=_('Проект'))
	profile_from = models.ForeignKey(UserProfile, on_delete=models.CASCADE, verbose_name=_('Автор отзыва'), related_name='profile_from')
	profile_to = models.ForeignKey(UserProfile, on_delete=models.CASCADE, verbose_name=_('Кому оставили отзыв'), related_name='profile_to')
	message = models.TextField(verbose_name=_('Отзыв'))
	evaluation = models.PositiveIntegerField(default=3, verbose_name=_('Оценка'), validators=[MaxValueValidator(5),])
	date_time = models.DateTimeField(auto_now_add=True, verbose_name=_('Дата и время добавления отзыва'))

	def __str__(self):
		return self.message

	class Meta:
		verbose_name = _('Отзыв')
		verbose_name_plural = _('Отзывы')