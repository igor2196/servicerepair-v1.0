from django import forms
from userProfile.models import Specialization
from .models import *


class ProjectForm(forms.ModelForm):
	class Meta:
		model = Project
		fields = ('name', 'description', 'town', 'specialization',
			'money', 'term', 'relevant_to', 'estimate')

FilesInlineFormSet = forms.inlineformset_factory(
	Project,
	FileProject,
	fields = '__all__',
	max_num = 3
)

class RateForm(forms.ModelForm):
	class Meta:
		model = Rate
		fields = ('term', 'money', 'message')

class SelectPerformerForm(forms.Form):
	performer = forms.CharField(required=True, max_length=50, widget=forms.HiddenInput())
	project = forms.CharField(required=True, max_length=50, widget=forms.HiddenInput())

class ReviewForm(forms.ModelForm):
	class Meta:
		model = Review
		fields = ('message', 'evaluation', )

class SpecializationFilterForm(forms.Form):
	is_only_my = forms.BooleanField(label='Только мои специализации', required=False)
	my_project = forms.BooleanField(label='Мои проекты', required=False)
	skill = forms.ModelMultipleChoiceField(
		required=False,
		queryset = Specialization.objects.all(),
		widget  = forms.SelectMultiple,
	)

class SearchForm(forms.Form):
	q = forms.CharField(label='Поиск')