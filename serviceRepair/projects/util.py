def estimate_name(instance, filename):
	return '/'.join(['users', str(instance.customer.user.id), 'project_'+ str(instance.id), filename])

def file_name(instance, filename):
	return '/'.join(['users', str(instance.project.customer.user.id), 'project_'+ str(instance.project.id), filename])