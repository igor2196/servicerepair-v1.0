from django.contrib import admin

from .models import *


@admin.register(ProjectStatus)
class ProjectAdmin(admin.ModelAdmin):
	list_display = ('name', 'id')
	search_fields = ('name', )

class FileProjectInline(admin.TabularInline):
	model = FileProject
	autocomplete_fields = ['project', ]

@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
	list_display = ('name', 'customer', 'specialization', 'money')
	search_fields = ('name', 'customer__user__username', 'specialization__name', 'specialization__group__name')
	list_filter = ('status__name', 'town__name')
	inlines = [FileProjectInline,]
	autocomplete_fields = ['specialization', 'customer', 'performer', 'town', 'manager']

@admin.register(Rate)
class RateAdmin(admin.ModelAdmin):
	list_display = ('project', 'performer', 'term', 'money', 'message')
	search_fields = ('project__name', 'performerr__user__username', 'message')
	autocomplete_fields = ['project', 'performer']

@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
	list_display = ('message', 'project', 'profile_from', 'profile_to')
	search_fields = ('message', 'project__name', 'profile_from__user__username', 'profile_to__user__username')
	list_filter = ('evaluation',)
	autocomplete_fields = ['project', 'profile_from', 'profile_to']