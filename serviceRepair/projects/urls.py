from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
	url(r'^add/$', views.add, name='addProject'),
	url(r'^my/$', views.my, name='myProject'),
	url(r'^board/$', views.board, name='boardProject'),
	url(r'^rates/$', views.myRate, name='myRate'),
	url(r'^myreviews/$', views.myReviews, name='myReviews'),
	url(r'^selectperformer/$', views.selectPerformer, name='selectPerformer'),
	url(r'^remove/$', views.removeProject, name='removeProject'),
	url(r'^close/$', views.closeProject, name='closeProject'),
	url(r'^end/$', views.endProject, name='endProject'),
	url(r'^extend/$', views.extendProject, name='extendProject'),
	path('<int:id>/', views.project, name='project'),
	path('addreview/<int:id>/', views.addReview, name='addReview'),
]