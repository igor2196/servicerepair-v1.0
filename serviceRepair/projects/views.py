from django.shortcuts import render, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from django.utils import timezone
from django.shortcuts import get_object_or_404
from django.http import Http404
from django.contrib import messages
from django.db.models import Q, Count, Max, Case, When
from django.utils.translation import gettext_lazy as _
from defaultApp.templatetags.auth_extras import has_group
from userProfile.models import UserProfile, Specialization
from messages.models import Messages
from .forms import *
from .models import *


def add(request):

	if not request.user.is_authenticated or not has_group(request.user, "Заказчик"):
		return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

	if request.method == 'POST':
		projectForm = ProjectForm(request.POST, request.FILES)
		projectFilesFormSet = FilesInlineFormSet(request.POST, request.FILES)

		if projectForm.is_valid() and projectFilesFormSet.is_valid():
			project_new = projectForm.save(commit=False)
			project_new.customer = request.user.userprofile
			project_new.save()

			files = projectFilesFormSet.save(commit=False)

			for file in files:
				file.project = project_new
				file.save()
			return redirect('myProject')
		else:
			messages.error(request, _('Пожалуйста, исправьте ошибки.'))
	else:
		projectForm = ProjectForm()
		projectFilesFormSet = FilesInlineFormSet()

	context = {
		'pageName': _('Новый проект'),
		'activPage': 'AddProject',
		'projectForm': projectForm,
		'projectFilesFormSet': projectFilesFormSet,
	}

	return render(request, 'projects/add.html', context)


def my(request):

	#todo: фильтр по статусу и поиск по названию

	if not request.user.is_authenticated:
		return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

	if has_group(request.user, "Заказчик"):
		projects_list = Project.objects.filter(customer=request.user.userprofile).annotate(count_rate=Count('rate')).order_by('-date_time')

	if has_group(request.user, "Исполнитель"):
		projects_list = Project.objects.filter(performer=request.user.userprofile).order_by('-date_time')

	paginator = Paginator(projects_list, 5)
	page = request.GET.get('page', 1)

	try:
		projects = paginator.page(page)
	except PageNotAnInteger:
		projects = paginator.page(1)
	except EmptyPage:
		projects = paginator.page(paginator.num_pages)

	template = 'profile.html'
	template_inc = 'projects/my.html'

	context = {
		'activPage': 'MyProject',
		'template_inc': template_inc,
		'pageName': _('Мои проекты'),
		'projects': projects
	}

	return render(request, template, context)


def board(request):

	# фильтр по статусу

	search = request.GET.get('q')

	skills = request.GET.getlist('skill')

	my_project=None
	is_only_my=None

	if request.user.is_authenticated and has_group(request.user, "Заказчик"):
		my_project = request.GET.get('my_project')

	if request.user.is_authenticated and has_group(request.user, "Исполнитель"):
		is_only_my = request.GET.get('is_only_my')

	projects_list = Project.objects.filter(~Q(status=9)).annotate(count_rate=Count('rate')).order_by('-date_time')

	if search:
		projects_list = projects_list.filter(name__icontains=search)

	if skills and not is_only_my:
		projects_list = projects_list.filter(specialization__in=skills)
	elif is_only_my:
		projects_list = projects_list.filter(Q(specialization__in=request.user.userprofile.specializations.all()) | Q(specialization = request.user.userprofile.specialization))

	if my_project:
		projects_list = projects_list.filter(id__in=Project.objects.filter(customer=request.user.userprofile).values_list('id'))

	paginator = Paginator(projects_list, 10)
	page = request.GET.get('page', 1)

	try:
		projects = paginator.page(page)
	except PageNotAnInteger:
		projects = paginator.page(1)
	except EmptyPage:
		projects = paginator.page(paginator.num_pages)

	context = {
		'activPage': 'BoardProject',
		'pageName': _('Все проекты'),
		'specializationFilterForm': SpecializationFilterForm(request.GET),
		'searchForm': SearchForm(request.GET),
		'projects': projects
	}

	return render(request, 'projects/board.html', context)


def project(request, id=None):

	project = get_object_or_404(Project, pk=id)
	flagRate = False

	if project.status.id == 9 and project.customer != request.user.userprofile:
		raise Http404

	if request.method == 'POST':
		if request.user.is_authenticated and has_group(request.user, "Исполнитель"):
			if Rate.objects.filter(project=project, performer=request.user.userprofile).first() == None:

				rateForm = RateForm(request.POST, request.FILES)

				if rateForm.is_valid():
					rate_new = rateForm.save(commit=False)
					rate_new.project = project
					rate_new.performer = request.user.userprofile
					rate_new.save()
					messages.success(request, _('Ваша ставка успешно добавлена'))
				else:
					messages.error(request, _('Пожалуйста, исправьте ошибки.'))
			else:
				rateForm = RateForm()
	else:
		rateForm = RateForm()

	if request.user.is_authenticated and has_group(request.user, "Исполнитель"):
		flagRate = True if Rate.objects.filter(project=project, performer=request.user.userprofile).first() == None else False

	rates_list = Rate.objects.filter(project=project).order_by('-date_time')

	paginator = Paginator(rates_list, 10)
	page = request.GET.get('page', 1)

	try:
		rates = paginator.page(page)
	except PageNotAnInteger:
		rates = paginator.page(1)
	except EmptyPage:
		rates = paginator.page(paginator.num_pages)

	context = {
		'pageName': _('Проект'),
		'project': project,
		'rates': rates,
		'rateForm': rateForm,
		'flagRate': flagRate,
		'selectPerformerForm': SelectPerformerForm({"project": project.id})
	}

	return render(request, 'projects/project.html', context)


def myRate(request, id=None):

	#todo: фильтр по статусу и поиск по названию

	if not request.user.is_authenticated or not has_group(request.user, "Исполнитель"):
		return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

	rates_list = Rate.objects.filter(performer=request.user.userprofile).order_by('-date_time')

	paginator = Paginator(rates_list, 5)
	page = request.GET.get('page', 1)

	try:
		rates = paginator.page(page)
	except PageNotAnInteger:
		rates = paginator.page(1)
	except EmptyPage:
		rates = paginator.page(paginator.num_pages)


	template = 'profile.html'
	template_inc = 'projects/myRate.html'

	context = {
		'activPage': 'MyRate',
		'template_inc': template_inc,
		'pageName': _('Мои ставки'),
		'rates': rates
	}

	return render(request, template, context)


def selectPerformer(request):

	if request.method == 'POST':
		if not request.user.is_authenticated or not has_group(request.user, "Заказчик"):
			raise Http404

		project = get_object_or_404(Project, pk=request.POST.get('project', -1))

		if project.customer != request.user.userprofile:
			raise Http404

		performer = get_object_or_404(UserProfile, pk=request.POST.get('performer', -1))

		if not has_group(performer.user, "Исполнитель"):
			raise Http404

		project.performer = performer
		project.status = ProjectStatus.objects.get(pk=4)
		project.manager = UserProfile.objects.get(pk=6)
		project.save()

		message = Messages()
		message.project = project
		message.profile = project.manager
		message.message = _("Здравствуйте, я ваш менеджер проекта. Это ваше рабочее пространство проекта.")
		message.save()

	else:
		raise Http404

	return redirect('messages')


def removeProject(request):

	if request.method == 'POST':
		if not request.user.is_authenticated or not has_group(request.user, "Заказчик"):
			raise Http404

		project = get_object_or_404(Project, pk=request.POST.get('project', -1), status__id=9)

		if project.customer != request.user.userprofile:
			raise Http404

		project.delete()
	else:
		raise Http404

	return redirect('myProject')


def closeProject(request):

	if request.method == 'POST':
		if not request.user.is_authenticated or not has_group(request.user, "Заказчик"):
			raise Http404

		project = get_object_or_404(Project, pk=request.POST.get('project', -1))

		if project.customer != request.user.userprofile:
			raise Http404

		project.status = ProjectStatus.objects.get(pk=7)
		project.save()
	else:
		raise Http404

	return redirect('myProject')


def endProject(request):

	if request.method == 'POST':
		if not request.user.is_authenticated or not has_group(request.user, "Заказчик"):
			raise Http404

		project = get_object_or_404(Project, ~Q(customer=None), pk=request.POST.get('project', -1))

		if project.customer != request.user.userprofile:
			raise Http404

		project.status = ProjectStatus.objects.get(pk=5)
		project.save()
	else:
		raise Http404

	return redirect('myProject')


def extendProject(request):

	if request.method == 'POST':
		if not request.user.is_authenticated or not has_group(request.user, "Заказчик"):
			raise Http404

		project = get_object_or_404(Project, pk=request.POST.get('project', -1), status__id=8)

		if project.customer != request.user.userprofile:
			raise Http404

		project.date_time = timezone.now()
		project.relevant_to = timezone.now() + timezone.timedelta(days=30)
		project.status = ProjectStatus.objects.get(pk=2)
		project.save()
	else:
		raise Http404

	return redirect('myProject')


def addReview(request, id=None):

	if not request.user.is_authenticated:
		return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

	if has_group(request.user, "Заказчик"):
		project = get_object_or_404(Project, Q(status__id=5) | Q(status__id=7), ~Q(performer=None), customer=request.user.userprofile, pk=id)
		profile_to = project.performer

	elif has_group(request.user, "Исполнитель"):
		project = get_object_or_404(Project, Q(status__id=5) | Q(status__id=7), performer=request.user.userprofile, pk=id)
		profile_to = project.customer

	if Review.objects.filter(profile_from=request.user.userprofile, project=project).count() >0:
		raise Http404

	if request.method == 'POST':
		reviewForm = ReviewForm(request.POST, request.FILES)
		if reviewForm.is_valid():
			review_new = reviewForm.save(commit=False)
			review_new.project = project
			review_new.profile_from = request.user.userprofile
			review_new.profile_to = profile_to
			review_new.save()

			if Review.objects.filter(project=project).count() == 2:
				project.status = ProjectStatus.objects.get(pk=6)
				project.save()

			return redirect('myReviews')
		else:
			messages.error(request, _('Пожалуйста, исправьте ошибки.'))
	else:
		reviewForm = ReviewForm()

	template = 'profile.html'
	template_inc = 'projects/addReview.html'

	context = {
		'activPage': 'MyReviews',
		'template_inc': template_inc,
		'pageName': _('Оставить отзыв'),
		'reviewForm': reviewForm,
		'project': project,
		'profile_to': profile_to
	}

	return render(request, template, context)

def myReviews(request, id=None):

	if not request.user.is_authenticated:
		return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

	if has_group(request.user, "Заказчик"):
		projects_review = Project.objects.filter(Q(status__id=5) | Q(status__id=7), ~Q(performer=None), customer=request.user.userprofile).annotate(count_rate=Count('review', filter=Q(review__profile_from=request.user.userprofile))).filter(count_rate=0)

	elif has_group(request.user, "Исполнитель"):
		projects_review = Project.objects.filter(Q(status__id=5) | Q(status__id=7), performer=request.user.userprofile).annotate(count_rate=Count('review', filter=Q(review__profile_from=request.user.userprofile))).filter(count_rate=0)

	reviews_list = Review.objects.filter(profile_to=request.user.userprofile)

	paginator = Paginator(reviews_list, 10)
	page = request.GET.get('page', 1)

	try:
		reviews = paginator.page(page)
	except PageNotAnInteger:
		reviews = paginator.page(1)
	except EmptyPage:
		reviews = paginator.page(paginator.num_pages)

	template = 'profile.html'
	template_inc = 'projects/myReviews.html'

	context = {
		'activPage': 'MyReviews',
		'template_inc': template_inc,
		'pageName': _('Мои отзывы'),
		'projects_review': projects_review,
		'reviews': reviews
	}

	return render(request, template, context)