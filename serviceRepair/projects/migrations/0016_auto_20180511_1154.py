# Generated by Django 2.0.4 on 2018-05-11 08:54

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0015_rate_date_time'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='rate',
            name='active',
        ),
        migrations.RemoveField(
            model_name='rate',
            name='read',
        ),
    ]
