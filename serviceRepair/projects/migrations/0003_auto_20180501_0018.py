# Generated by Django 2.0.4 on 2018-04-30 21:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('userProfile', '0018_auto_20180428_2243'),
        ('projects', '0002_auto_20180501_0004'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='description',
            field=models.TextField(default='kkm', help_text='Описание должно быть достаточно подробным, чтобы заинтересовать заказчиков и дать им возможность оценить объем и стоимость работы. Внимание: быстрый зароботок, реклама сайтов, дубликаты и явно неподходящие по тематике проекты будут беспощадно удаляться!', verbose_name='Описание'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='project',
            name='town',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='userProfile.Towns', verbose_name='Город'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='project',
            name='term',
            field=models.PositiveIntegerField(help_text='Укажите срок в днях', verbose_name='Желаемый срок'),
        ),
    ]
