from django import template
from django.db.models import Q, Count
from defaultApp.templatetags.auth_extras import has_group
from projects.models import *

register = template.Library()

@register.filter(name='get_review_count')
def get_review_count(userprofile):

	count_message = 0

	if has_group(userprofile.user, "Заказчик"):
		count_message = Project.objects.filter(Q(status__id=5) | Q(status__id=7), ~Q(performer=None), customer=userprofile).annotate(count_rate=Count('review', filter=Q(review__profile_from=userprofile))).filter(count_rate=0).count()

	elif has_group(userprofile.user, "Исполнитель"):
		count_message = Project.objects.filter(Q(status__id=5) | Q(status__id=7), performer=userprofile).annotate(count_rate=Count('review', filter=Q(review__profile_from=userprofile))).filter(count_rate=0).count()

	return count_message