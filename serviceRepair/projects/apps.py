from django.utils.translation import gettext_lazy as _
from django.apps import AppConfig


class ProjectsConfig(AppConfig):
	name = 'projects'
	verbose_name = _('Проекты')