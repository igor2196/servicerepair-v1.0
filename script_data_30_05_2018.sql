--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2018-05-30 11:09:56

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- TOC entry 3147 (class 0 OID 24627)
-- Dependencies: 207
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
5	pbkdf2_sha256$100000$plZsLZukc81c$Lzt0LqZyk2jVy0pK5x0V+p0gIZRvgrpeAakS4VBa1Mk=	\N	f	manager	Елена	Карандина		t	t	2018-05-12 14:47:18+03
19	pbkdf2_sha256$100000$esdmPR7COpdF$PvrtiH+4C1ehwWtd1iFq2XBldxCVmdhqXhIU42i3HnY=	\N	f	djon	фывф	фывфы	ermolov962104@gmail.com	f	f	2018-05-27 22:20:20.428108+03
2	pbkdf2_sha256$100000$5xZpXENNAMab$SBXabtowPDbJc4M2g4DXTdGiY0TPhe562qiXjsxDCrA=	2018-05-27 22:48:25.602208+03	f	igor	Игорь	Павлов	dadas@sda.asd	f	t	2018-03-28 22:58:08+03
3	pbkdf2_sha256$100000$QauO0s4m52yr$1q3j8EJK8+W7BmHmMliMgS4XZTKjBaRLv0anwXEw3ms=	2018-05-27 22:51:50.339358+03	f	viktor	Виктор	Степанов	dsadsa@sada.afa	f	t	2018-03-28 22:58:31+03
1	pbkdf2_sha256$100000$6GbDHBropIdf$gGzLGv45fI9eJ/u84s8nzVyk2O1ttxKORo7TrAT98Po=	2018-05-28 00:00:39.30658+03	t	admin	Игорь	Ермолов	ermolov962104@gmail.com	t	t	2018-01-04 17:48:10+03
\.


--
-- TOC entry 3184 (class 0 OID 33277)
-- Dependencies: 244
-- Data for Name: projects_projectstatus; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY projects_projectstatus (id, name) FROM stdin;
2	Прием ставок
4	Выполнение проекта
5	Обмен отзывами
6	Завершен
8	Истек срок
9	Ожидает оплаты
7	Закрыт
\.


--
-- TOC entry 3172 (class 0 OID 33028)
-- Dependencies: 232
-- Data for Name: userProfile_specializationgroup; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "userProfile_specializationgroup" (id, name, sort) FROM stdin;
10	Комплексные работы	1
8	Строительно-монтажные работы	2
7	Отделочные работы	3
9	Разное	4
\.


--
-- TOC entry 3170 (class 0 OID 33020)
-- Dependencies: 230
-- Data for Name: userProfile_specialization; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "userProfile_specialization" (id, name, group_id) FROM stdin;
24	Укладка плитки	7
25	Электрика	7
26	Сантехника	7
27	Монтаж гипсокартона	7
28	Малярно-штукатурные работы	7
29	Укладка паркета	7
30	Кладка кирпича или камня	8
31	Обработка древесины	8
32	Монолитные работы	8
33	Укладка кровли	8
36	Капитальный ремонт	10
37	Перепланировка помещения	10
38	Евроремонт	10
35	Косметический ремонт	10
34	Ремонт под ключ	10
\.


--
-- TOC entry 3162 (class 0 OID 24782)
-- Dependencies: 222
-- Data for Name: userProfile_statuses; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "userProfile_statuses" (id, name) FROM stdin;
1	Свободен для работы
2	Немного занят
3	Сильно занят
4	Временно не работаю
5	В отпуске
\.


--
-- TOC entry 3160 (class 0 OID 24768)
-- Dependencies: 220
-- Data for Name: userProfile_towns; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "userProfile_towns" (id, name) FROM stdin;
1	Ростов-на-Дону
2	Таганрог
\.


--
-- TOC entry 3158 (class 0 OID 24753)
-- Dependencies: 218
-- Data for Name: userProfile_userprofile; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "userProfile_userprofile" (id, "birthDate", image, phone, user_id, town_id, url, status_id, summary, skype, specialization_id, info) FROM stdin;
2	2018-03-20	users/2/Qem2-vKSRVw.jpg	\N	2	1	\N	1		\N	32	\N
3	\N	users/3/anonim.png	\N	3	1	http://hi.com	1	\N	\N	\N	\N
1	1996-11-22		+7 (343)-24-34-324	1	1	http://asdasd.sds	1	sdsd1dfknkmkm jhjhjn vg	\N	29	\N
6	\N		\N	5	\N	\N	1	\N	\N	\N	\N
20	\N		+7 (123)-21-31-231	19	1	\N	1	\N	\N	38	asdasda
\.


--
-- TOC entry 3182 (class 0 OID 33268)
-- Dependencies: 242
-- Data for Name: projects_project; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY projects_project (id, name, money, term, relevant_to, date_time, estimate, customer_id, performer_id, specialization_id, status_id, description, town_id, manager_id) FROM stdin;
12	Сантехника в доме	15000.00	10	2018-05-31	2018-05-12 15:59:52+03		3	2	26	6	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.	1	6
15	Проект	5000.00	15	2018-05-25	2018-05-17 12:07:26+03		3	1	38	4	фывыовоыифовы выфтвоыфтво ытвот фыово фыво фыовыофт	2	6
13	Укладка плитки на кухне	6000.00	4	2018-05-24	2018-05-14 00:30:28+03		3	1	24	5	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.	1	6
14	Укладка паркета в гостинной	13000.00	13	2018-05-25	2018-05-14 00:39:00+03		3	\N	29	9	ыыыыыыыыыыыыыыыыыыыыыыыыы ыыыыыыыыыыыыыыыыыыыыыыыввв ввввввввввввввввввввввввввввввввввв ввввввввввв \r\n вввввввввввввввввввввввввввввввввв ввв вввввввввввввввввввввввввввввввв ввввввввввввввввввввввв ввввввввввввв ввввввввввввввввв ввввввввввввввввввввввввввв вввввввввв вввввввввввввввввввв ввввввввввввввввв \r\n вввввввввввввввввввввввввввввввввввв вввввввввввввввввввввввв	1	\N
16	ывыфвл	6000.00	5	2018-05-29	2018-05-27 22:43:00+03		3	1	36	5	фывфывф	1	6
\.


--
-- TOC entry 3178 (class 0 OID 33137)
-- Dependencies: 238
-- Data for Name: Users_messages_messages; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "Users_messages_messages" (id, message, date_time, profile_id, project_id, read_customer, read_performer) FROM stdin;
18	Здравствуйте, я ваш менеджер проекта. Это ваше рабочее пространство проекта.	2018-05-12 16:05:43.826991+03	6	12	t	t
19	Новое сообщение	2018-05-12 17:36:51.466619+03	3	12	f	t
20	Конечно	2018-05-12 17:37:26.225613+03	2	12	t	f
21	Здравствуйте, я ваш менеджер проекта. Это ваше рабочее пространство проекта.	2018-05-16 10:33:37.184293+03	6	13	t	t
22	Добрый день, какой материал необходимо закупить??	2018-05-16 10:38:00.225476+03	3	13	f	t
23	Здравствуйте, пока еще все есть. Сегодня доложим одну из стен тогда напишу.	2018-05-16 10:39:26.659069+03	1	13	t	f
25	Здравствуйте, я ваш менеджер проекта. Это ваше рабочее пространство проекта.	2018-05-17 12:09:16.032932+03	6	15	t	t
26	sadnsajbd	2018-05-17 12:11:43.725724+03	1	15	t	f
27	asdsad	2018-05-17 12:11:56.434606+03	3	15	f	t
28	sadnsajbd	2018-05-17 12:12:00.866208+03	1	15	t	f
29	Здравствуйте, я ваш менеджер проекта. Это ваше рабочее пространство проекта.	2018-05-27 23:33:17.589866+03	6	16	f	t
24	Добрый день, какой материал необходимо закупить??	2018-05-16 10:39:33.783258+03	3	13	f	t
\.


--
-- TOC entry 3143 (class 0 OID 24609)
-- Dependencies: 203
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_group (id, name) FROM stdin;
2	Исполнитель
1	Заказчик
\.


--
-- TOC entry 3139 (class 0 OID 24591)
-- Dependencies: 199
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	estimate	typedesign
8	userProfile	userprofile
9	userProfile	towns
10	userProfile	statuses
11	portfolio	imagesportfolio
12	portfolio	portfolio
13	portfolio	fileportfolio
14	userProfile	specialization
15	userProfile	specializationgroup
16	serviceRepair.messages	messages
17	Users_messages	messages
18	Users_messages	dialogs
19	projects	projectstatus
20	projects	fileproject
21	projects	rate
22	projects	project
23	projects	review
\.


--
-- TOC entry 3141 (class 0 OID 24601)
-- Dependencies: 201
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add permission	2	add_permission
5	Can change permission	2	change_permission
6	Can delete permission	2	delete_permission
7	Can add group	3	add_group
8	Can change group	3	change_group
9	Can delete group	3	delete_group
10	Can add user	4	add_user
11	Can change user	4	change_user
12	Can delete user	4	delete_user
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can add type design	7	add_typedesign
20	Can change type design	7	change_typedesign
21	Can delete type design	7	delete_typedesign
22	Can add Профиль пользователя	8	add_userprofile
23	Can change Профиль пользователя	8	change_userprofile
24	Can delete Профиль пользователя	8	delete_userprofile
25	Can add Город	9	add_towns
26	Can change Город	9	change_towns
27	Can delete Город	9	delete_towns
28	Can add Статус	10	add_statuses
29	Can change Статус	10	change_statuses
30	Can delete Статус	10	delete_statuses
31	Can add Изображение профиля	11	add_imagesportfolio
32	Can change Изображение профиля	11	change_imagesportfolio
33	Can delete Изображение профиля	11	delete_imagesportfolio
34	Can add Портфолио	12	add_portfolio
35	Can change Портфолио	12	change_portfolio
36	Can delete Портфолио	12	delete_portfolio
37	Can add Файл проекта	13	add_fileportfolio
38	Can change Файл проекта	13	change_fileportfolio
39	Can delete Файл проекта	13	delete_fileportfolio
40	Can add Специализация	14	add_specialization
41	Can change Специализация	14	change_specialization
42	Can delete Специализация	14	delete_specialization
43	Can add Группа специализации	15	add_specializationgroup
44	Can change Группа специализации	15	change_specializationgroup
45	Can delete Группа специализации	15	delete_specializationgroup
46	Can add Сообщение	16	add_messages
47	Can change Сообщение	16	change_messages
48	Can delete Сообщение	16	delete_messages
49	Can add Сообщение	17	add_messages
50	Can change Сообщение	17	change_messages
51	Can delete Сообщение	17	delete_messages
52	Can add Диалог	18	add_dialogs
53	Can change Диалог	18	change_dialogs
54	Can delete Диалог	18	delete_dialogs
55	Can add Статус проекта	19	add_projectstatus
56	Can change Статус проекта	19	change_projectstatus
57	Can delete Статус проекта	19	delete_projectstatus
58	Can add Файл проекта	20	add_fileproject
59	Can change Файл проекта	20	change_fileproject
60	Can delete Файл проекта	20	delete_fileproject
61	Can add Ставка проекта	21	add_rate
62	Can change Ставка проекта	21	change_rate
63	Can delete Ставка проекта	21	delete_rate
64	Can add Проект	22	add_project
65	Can change Проект	22	change_project
66	Can delete Проект	22	delete_project
67	Can add Отзыв	23	add_review
68	Can change Отзыв	23	change_review
69	Can delete Отзыв	23	delete_review
\.


--
-- TOC entry 3145 (class 0 OID 24619)
-- Dependencies: 205
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- TOC entry 3149 (class 0 OID 24637)
-- Dependencies: 209
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
1	3	1
2	2	2
5	1	2
11	19	2
\.


--
-- TOC entry 3151 (class 0 OID 24645)
-- Dependencies: 211
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- TOC entry 3153 (class 0 OID 24705)
-- Dependencies: 213
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2018-01-04 21:39:36.948702+03	1	admin	2	[{"changed": {"fields": ["first_name", "last_name"]}}]	4	1
2	2018-01-04 22:54:06.86441+03	1	привет мир	1	[{"added": {}}]	7	1
3	2018-01-04 23:14:26.192138+03	1	привет мир	3		7	1
4	2018-01-04 23:17:04.52303+03	2	Огни большого города	1	[{"added": {}}]	7	1
5	2018-01-04 23:17:17.640289+03	3	Эта замечательная жизнь	1	[{"added": {}}]	7	1
6	2018-01-04 23:17:33.036336+03	4	В погоне за счастьем	1	[{"added": {}}]	7	1
7	2018-01-04 23:17:48.223217+03	5	Пробуждение	1	[{"added": {}}]	7	1
8	2018-01-04 23:52:46.017197+03	1	Заказчик	1	[{"added": {}}]	3	1
9	2018-01-04 23:53:12.038882+03	2	Исполнитель	1	[{"added": {}}]	3	1
10	2018-03-28 22:58:08.405591+03	2	igor	1	[{"added": {}}]	4	1
11	2018-03-28 22:58:31.713761+03	3	viktor	1	[{"added": {}}]	4	1
12	2018-03-28 22:58:55.974368+03	3	viktor	2	[{"changed": {"fields": ["groups"]}}]	4	1
13	2018-03-28 22:59:19.16364+03	2	igor	2	[{"changed": {"fields": ["groups"]}}]	4	1
14	2018-03-29 00:17:50.283927+03	2	igor	2	[{"changed": {"fields": ["first_name", "last_name", "email"]}}]	4	1
15	2018-03-29 00:18:13.644495+03	3	viktor	2	[{"changed": {"fields": ["first_name", "last_name", "email"]}}]	4	1
16	2018-03-29 00:36:37.190828+03	2	igor	2	[{"changed": {"fields": ["is_active"]}}]	4	1
17	2018-03-29 00:38:02.912441+03	2	igor	2	[{"changed": {"fields": ["is_active"]}}]	4	1
18	2018-03-31 19:06:32.814426+03	1	admin	1	[{"added": {}}]	8	1
19	2018-03-31 19:10:34.07718+03	2	igor	1	[{"added": {}}]	8	1
20	2018-03-31 19:10:43.795361+03	3	viktor	1	[{"added": {}}]	8	1
21	2018-03-31 19:45:10.342563+03	1	Ростов-на-Дону	1	[{"added": {}}]	9	1
22	2018-03-31 19:45:15.363224+03	2	Таганрог	1	[{"added": {}}]	9	1
23	2018-03-31 20:03:45.556716+03	1	Свободен для работы	1	[{"added": {}}]	10	1
24	2018-03-31 20:03:59.267504+03	2	Немного занят	1	[{"added": {}}]	10	1
25	2018-03-31 20:04:11.734176+03	3	Сильно занят	1	[{"added": {}}]	10	1
26	2018-03-31 20:04:26.092792+03	4	Временно не работаю	1	[{"added": {}}]	10	1
27	2018-03-31 20:04:37.536889+03	5	В отпуске	1	[{"added": {}}]	10	1
28	2018-04-01 00:31:41.758812+03	1	admin	2	[{"changed": {"fields": ["summary"]}}]	8	1
29	2018-04-01 00:32:56.200724+03	1	admin	2	[{"changed": {"fields": ["summary"]}}]	8	1
30	2018-04-01 01:10:01.476164+03	1	admin	2	[]	8	1
31	2018-04-01 01:18:17.711365+03	1	admin	2	[{"changed": {"fields": ["image"]}}]	8	1
32	2018-04-01 01:40:06.664813+03	1	admin	2	[{"changed": {"fields": ["image"]}}]	8	1
33	2018-04-01 01:40:57.963889+03	1	admin	2	[{"changed": {"fields": ["image"]}}]	8	1
34	2018-04-01 01:47:31.25042+03	1	admin	2	[{"changed": {"fields": ["image"]}}]	8	1
35	2018-04-01 01:57:37.740321+03	1	admin	2	[{"changed": {"fields": ["image"]}}]	8	1
36	2018-04-01 02:37:41.681532+03	2	Исполнитель	2	[{"changed": {"fields": ["permissions"]}}]	3	1
37	2018-04-01 02:37:54.009634+03	1	Заказчик	2	[{"changed": {"fields": ["permissions"]}}]	3	1
38	2018-04-08 20:51:36.82938+03	2	igor	2	[{"changed": {"fields": ["image"]}}]	8	1
39	2018-04-08 20:54:16.258436+03	2	dss	1	[{"added": {}}, {"added": {"name": "\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0430", "object": "ImagesPortfolio object"}}]	12	1
40	2018-04-08 20:54:48.187195+03	2	dss	2	[{"deleted": {"name": "\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0430", "object": "ImagesPortfolio object"}}]	12	1
41	2018-04-08 20:55:24.523829+03	2	igor	2	[{"changed": {"fields": ["image"]}}]	8	1
42	2018-04-08 23:02:18.677197+03	1	admin	2	[{"changed": {"fields": ["groups"]}}]	4	1
43	2018-04-08 23:02:38.983647+03	1	admin	2	[{"changed": {"fields": ["groups"]}}]	4	1
44	2018-04-08 23:03:06.199638+03	1	admin	2	[{"changed": {"fields": ["groups"]}}]	4	1
45	2018-04-17 19:59:42.897452+03	2	dss	2	[{"added": {"name": "\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0430", "object": "ImagesPortfolio object"}}]	12	1
46	2018-04-17 20:01:58.825354+03	2	igor	2	[{"changed": {"fields": ["image"]}}]	8	1
47	2018-04-17 20:02:23.103402+03	2	igor	2	[{"changed": {"fields": ["image"]}}]	8	1
48	2018-04-17 20:03:21.524094+03	2	igor	2	[{"changed": {"fields": ["image"]}}]	8	1
49	2018-04-17 20:03:36.090914+03	2	igor	2	[{"changed": {"fields": ["image"]}}]	8	1
50	2018-04-17 20:04:12.404971+03	2	dss	2	[{"changed": {"name": "\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0430", "object": "ImagesPortfolio object", "fields": ["image"]}}]	12	1
51	2018-04-17 20:04:53.272227+03	2	dss	2	[{"changed": {"name": "\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0430", "object": "ImagesPortfolio object", "fields": ["image"]}}]	12	1
52	2018-04-17 20:05:29.27143+03	2	dss	2	[{"changed": {"name": "\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0430", "object": "ImagesPortfolio object", "fields": ["image"]}}]	12	1
53	2018-04-17 20:06:05.732516+03	2	dss	2	[{"changed": {"name": "\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0430", "object": "ImagesPortfolio object", "fields": ["image"]}}]	12	1
54	2018-04-17 20:07:40.809618+03	2	dss	2	[{"changed": {"name": "\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0430", "object": "ImagesPortfolio object", "fields": ["image"]}}]	12	1
55	2018-04-17 20:10:22.643503+03	2	dss	2	[{"changed": {"name": "\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0430", "object": "ImagesPortfolio object", "fields": ["image"]}}]	12	1
56	2018-04-17 20:10:44.173831+03	2	dss	2	[{"deleted": {"name": "\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0430", "object": "ImagesPortfolio object"}}]	12	1
57	2018-04-17 20:12:40.422205+03	2	dss	2	[{"added": {"name": "\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0430", "object": "ImagesPortfolio object"}}]	12	1
58	2018-04-17 20:13:09.534013+03	2	dss	2	[{"deleted": {"name": "\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0430", "object": "ImagesPortfolio object"}}]	12	1
59	2018-04-17 20:14:14.007197+03	2	dss	2	[{"added": {"name": "\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0430", "object": "\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435"}}]	12	1
139	2018-04-26 22:52:40.882016+03	29	Укладка паркета	1	[{"added": {}}]	14	1
60	2018-04-17 20:14:40.599818+03	2	dss	2	[{"deleted": {"name": "\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0430", "object": "\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435"}}]	12	1
61	2018-04-17 20:15:06.124717+03	2	dss	2	[{"added": {"name": "\\u0424\\u0430\\u0439\\u043b \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0430", "object": "\\u0442\\u0435\\u0442\\u0441\\u044b"}}]	12	1
62	2018-04-17 20:15:48.145272+03	2	dss	2	[{"deleted": {"name": "\\u0424\\u0430\\u0439\\u043b \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0430", "object": "\\u0442\\u0435\\u0442\\u0441\\u044b"}}]	12	1
63	2018-04-17 20:17:01.125604+03	2	dss	2	[{"added": {"name": "\\u0424\\u0430\\u0439\\u043b \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0430", "object": "\\u0442\\u0442\\u043e"}}]	12	1
64	2018-04-17 20:18:24.374561+03	2	dss	2	[{"deleted": {"name": "\\u0424\\u0430\\u0439\\u043b \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0430", "object": "\\u0442\\u0442\\u043e"}}]	12	1
65	2018-04-17 20:31:01.957174+03	1	admin	2	[{"changed": {"fields": ["image"]}}]	8	1
66	2018-04-17 21:23:33.326687+03	2	dss	2	[{"changed": {"fields": ["profile"]}}]	12	1
67	2018-04-17 21:23:49.999336+03	2	dss	2	[{"changed": {"fields": ["profile"]}}]	12	1
68	2018-04-17 21:27:03.959157+03	2	dss	2	[{"changed": {"fields": ["profile"]}}]	12	1
69	2018-04-17 21:27:16.042136+03	2	dss	2	[{"changed": {"fields": ["profile"]}}]	12	1
70	2018-04-17 21:27:25.666886+03	2	dss	2	[{"changed": {"fields": ["profile"]}}]	12	1
71	2018-04-17 21:36:53.982877+03	3	dss	3		12	1
72	2018-04-17 22:40:59.136429+03	4	тест	2	[{"changed": {"fields": ["profile"]}}]	12	1
73	2018-04-17 22:41:08.413998+03	2	dss	2	[{"changed": {"fields": ["profile"]}}]	12	1
74	2018-04-17 22:41:23.1406+03	2	dss	2	[{"changed": {"fields": ["profile"]}}]	12	1
75	2018-04-17 22:41:31.185249+03	4	тест	2	[{"changed": {"fields": ["profile"]}}]	12	1
76	2018-04-18 01:31:48.407556+03	5	фывфы	1	[{"added": {}}]	12	1
77	2018-04-18 01:32:03.076432+03	6	вапавп	1	[{"added": {}}]	12	1
78	2018-04-18 01:33:02.058759+03	7	арвапвп	1	[{"added": {}}]	12	1
79	2018-04-18 01:35:43.946031+03	8	fgdfgfd	1	[{"added": {}}]	12	1
80	2018-04-18 01:36:02.613187+03	9	sdfsdf	1	[{"added": {}}]	12	1
81	2018-04-18 01:47:32.06576+03	10	рпарап	1	[{"added": {}}]	12	1
82	2018-04-18 03:11:30.69539+03	11	vgvg	1	[{"added": {}}]	12	1
83	2018-04-18 03:12:19.552797+03	11	vgvg	3		12	1
84	2018-04-18 03:12:19.561803+03	10	рпарап	3		12	1
85	2018-04-18 03:12:19.564305+03	9	sdfsdf	3		12	1
86	2018-04-18 03:12:19.566306+03	8	fgdfgfd	3		12	1
87	2018-04-18 03:12:19.568307+03	7	арвапвп	3		12	1
88	2018-04-18 03:12:19.570309+03	6	вапавп	3		12	1
89	2018-04-18 03:12:19.572311+03	5	фывфы	3		12	1
90	2018-04-18 03:12:19.574311+03	4	тест	3		12	1
91	2018-04-18 18:32:46.656688+03	14	sda	2	[{"added": {"name": "\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0430", "object": "\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435"}}]	12	1
92	2018-04-24 00:58:06.392747+03	1	Отделочные работы	1	[{"added": {}}]	15	1
93	2018-04-24 00:58:33.369952+03	2	Строительно-монтажные работы	1	[{"added": {}}]	15	1
94	2018-04-24 00:59:13.756142+03	1	Гипсокартонщик	1	[{"added": {}}]	14	1
95	2018-04-24 00:59:34.703648+03	2	Плиточник	1	[{"added": {}}]	14	1
96	2018-04-24 01:00:11.476466+03	2	Плиточник	2	[]	14	1
97	2018-04-24 01:00:22.374354+03	3	Электрик	1	[{"added": {}}]	14	1
98	2018-04-24 01:00:44.053753+03	4	Маляр-штукатур	1	[{"added": {}}]	14	1
99	2018-04-24 01:01:13.741734+03	5	Сантехник	1	[{"added": {}}]	14	1
100	2018-04-24 01:01:32.881393+03	6	Паркетчик	1	[{"added": {}}]	14	1
101	2018-04-24 01:02:34.823946+03	7	Каменьщик	1	[{"added": {}}]	14	1
102	2018-04-24 01:02:49.727492+03	8	Специалист по камню ( мрамор, гранит )	1	[{"added": {}}]	14	1
103	2018-04-24 01:03:13.021499+03	9	Плотник	1	[{"added": {}}]	14	1
104	2018-04-24 01:03:32.520723+03	10	Кровельщик	1	[{"added": {}}]	14	1
105	2018-04-24 01:03:54.879055+03	11	Монолитчик	1	[{"added": {}}]	14	1
106	2018-04-24 01:03:58.173642+03	11	Монолитчик	2	[]	14	1
107	2018-04-24 01:04:25.645947+03	12	Прораб	1	[{"added": {}}]	14	1
108	2018-04-24 03:04:23.164056+03	2	Строительно-монтажные работы	3		15	1
109	2018-04-24 03:04:23.223419+03	1	Отделочные работы	3		15	1
110	2018-04-24 04:10:24.837679+03	3	jkkjk	1	[{"added": {}}]	15	1
111	2018-04-24 04:10:28.987748+03	13	jnjn	1	[{"added": {}}]	14	1
112	2018-04-24 04:10:35.413401+03	14	njnj	1	[{"added": {}}]	14	1
113	2018-04-24 22:35:33.427347+03	4	ура	1	[{"added": {}}]	15	1
114	2018-04-24 22:35:35.561952+03	14	njnj	2	[{"changed": {"fields": ["group"]}}]	14	1
115	2018-04-25 01:36:34.20459+03	15	mkmk	1	[{"added": {}}]	14	1
116	2018-04-25 01:36:55.584835+03	16	qew1	1	[{"added": {}}]	14	1
117	2018-04-25 01:45:38.262441+03	17	1	1	[{"added": {}}]	14	1
118	2018-04-25 01:45:47.712103+03	18	asdsadsadasdsadasda	1	[{"added": {}}]	14	1
119	2018-04-25 01:45:52.174667+03	19	asdsadsadasdsadasda	1	[{"added": {}}]	14	1
120	2018-04-25 01:45:57.382165+03	20	asdsadsadasdsadasda	1	[{"added": {}}]	14	1
121	2018-04-25 01:46:11.31882+03	21	asdsadsadasdsadasda	1	[{"added": {}}]	14	1
122	2018-04-25 02:00:30.840297+03	5	mkmk	1	[{"added": {}}]	15	1
123	2018-04-25 02:00:43.771187+03	22	wxdxd	1	[{"added": {}}]	14	1
124	2018-04-25 02:01:11.406593+03	6	яша	1	[{"added": {}}]	15	1
125	2018-04-25 02:01:16.254159+03	23	ьлтьлл	1	[{"added": {}}]	14	1
126	2018-04-25 02:01:33.704719+03	5	юра	2	[{"changed": {"fields": ["name"]}}]	15	1
127	2018-04-26 22:47:49.01592+03	6	яша	3		15	1
128	2018-04-26 22:47:49.202327+03	5	юра	3		15	1
129	2018-04-26 22:47:49.207326+03	4	ура	3		15	1
130	2018-04-26 22:47:49.211329+03	3	jkkjk	3		15	1
131	2018-04-26 22:48:58.23684+03	7	Отделочные работы	1	[{"added": {}}]	15	1
132	2018-04-26 22:49:22.840644+03	8	Строительно-монтажные работы	1	[{"added": {}}]	15	1
133	2018-04-26 22:49:26.462275+03	9	Разное	1	[{"added": {}}]	15	1
134	2018-04-26 22:50:24.381082+03	24	Укладка плитки	1	[{"added": {}}]	14	1
135	2018-04-26 22:50:43.186726+03	25	Электрика	1	[{"added": {}}]	14	1
136	2018-04-26 22:50:58.067617+03	26	Сантехника	1	[{"added": {}}]	14	1
137	2018-04-26 22:51:26.603108+03	27	Монтаж гипсокартона	1	[{"added": {}}]	14	1
138	2018-04-26 22:52:13.071984+03	28	Малярно-штукатурные работы	1	[{"added": {}}]	14	1
140	2018-04-26 22:53:34.108847+03	30	Кладка кирпича или камня	1	[{"added": {}}]	14	1
141	2018-04-26 22:54:16.242307+03	31	Обработка древесины	1	[{"added": {}}]	14	1
142	2018-04-26 22:55:07.285983+03	32	Монолитные работы	1	[{"added": {}}]	14	1
143	2018-04-26 22:55:50.343395+03	33	Укладка кровли	1	[{"added": {}}]	14	1
144	2018-04-26 22:56:05.929944+03	34	Ремонт под ключ	1	[{"added": {}}]	14	1
145	2018-04-26 22:57:48.866944+03	35	Косметический ремонт	1	[{"added": {}}]	14	1
146	2018-04-26 22:58:36.389182+03	10	Комплексные работы	1	[{"added": {}}]	15	1
147	2018-04-26 22:58:44.51923+03	36	Капитальный ремонт	1	[{"added": {}}]	14	1
148	2018-04-26 22:59:03.613304+03	37	Перепланировка помещения	1	[{"added": {}}]	14	1
149	2018-04-26 23:00:34.343575+03	38	Евроремонт	1	[{"added": {}}]	14	1
150	2018-04-26 23:16:20.138523+03	10	Комплексные работы	2	[{"changed": {"fields": ["sort"]}}]	15	1
151	2018-04-26 23:16:28.137931+03	9	Разное	2	[{"changed": {"fields": ["sort"]}}]	15	1
152	2018-04-26 23:16:36.757999+03	8	Строительно-монтажные работы	2	[{"changed": {"fields": ["sort"]}}]	15	1
153	2018-04-26 23:16:43.782773+03	7	Отделочные работы	2	[{"changed": {"fields": ["sort"]}}]	15	1
154	2018-04-26 23:16:49.092262+03	9	Разное	2	[{"changed": {"fields": ["sort"]}}]	15	1
155	2018-04-26 23:19:14.082342+03	9	Разное	2	[]	15	1
156	2018-04-26 23:20:10.485575+03	35	Косметический ремонт	2	[{"changed": {"fields": ["group"]}}]	14	1
157	2018-04-26 23:20:21.006002+03	34	Ремонт под ключ	2	[{"changed": {"fields": ["group"]}}]	14	1
158	2018-04-27 23:17:05.991766+03	2	ыфвывф	1	[{"added": {}}]	16	1
159	2018-04-27 23:17:16.873726+03	3	ывыфвфыв	1	[{"added": {}}]	16	1
160	2018-04-27 23:39:10.480424+03	3	ывыфвфыв	3		16	1
161	2018-04-27 23:39:10.517368+03	2	ыфвывф	3		16	1
162	2018-04-28 00:09:33.39736+03	1	Dialogs object (1)	1	[{"added": {}}]	18	1
163	2018-04-28 00:12:13.329294+03	2	Диалог admin и igor	1	[{"added": {}}]	18	1
164	2018-04-28 00:17:06.124262+03	1	jnjnjnj	1	[{"added": {}}]	17	1
165	2018-04-28 00:30:23.731599+03	2	kmk	1	[{"added": {}}]	17	1
166	2018-04-28 00:34:18.622761+03	3	111	1	[{"added": {}}]	17	1
167	2018-04-28 01:20:14.903429+03	4	12332	1	[{"added": {}}]	17	1
168	2018-04-28 02:22:40.576252+03	3	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo, fuga aut quis a, earum odit molestias itaque quam quaerat nulla architecto ea sequi accusantium sapiente fugiat sint et, culpa pariatur.	2	[{"changed": {"fields": ["message"]}}]	17	1
169	2018-04-28 02:22:59.899943+03	2	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo, fuga aut quis a, earum odit molestias itaque quam quaerat nulla architecto ea sequi accusantium sapiente fugiat sint et, culpa pariatur.	2	[{"changed": {"fields": ["message"]}}]	17	1
170	2018-04-28 20:29:43.645706+03	4		3		8	1
171	2018-04-28 20:30:03.253894+03	4		3		4	1
172	2018-04-28 20:31:10.851776+03	1	admin	2	[{"changed": {"fields": ["specialization"]}}]	8	1
173	2018-04-28 22:44:42.408986+03	2	Диалог admin и igor	2	[{"changed": {"fields": ["delete"]}}]	18	1
174	2018-04-28 22:44:57.130376+03	2	Диалог admin и igor	2	[{"changed": {"fields": ["delete"]}}]	18	1
175	2018-04-28 22:45:06.263829+03	2	Диалог admin и igor	2	[{"changed": {"fields": ["delete"]}}]	18	1
176	2018-04-28 23:09:05.107892+03	1	Диалог viktor и admin	2	[{"changed": {"fields": ["delete"]}}]	18	1
177	2018-04-28 23:09:35.813258+03	1	Диалог viktor и admin	2	[]	18	1
178	2018-04-28 23:22:45.651388+03	5	nnj	1	[{"added": {}}]	17	1
179	2018-04-28 23:25:32.376766+03	2	Диалог admin и igor	2	[{"changed": {"fields": ["delete"]}}]	18	1
180	2018-04-28 23:26:24.663731+03	2	Диалог admin и igor	2	[{"changed": {"fields": ["delete"]}}]	18	1
181	2018-04-28 23:30:59.567704+03	2	Диалог admin и igor	2	[{"changed": {"fields": ["delete"]}}]	18	1
182	2018-04-28 23:36:07.056595+03	2	Диалог admin и igor	2	[{"changed": {"fields": ["delete"]}}]	18	1
183	2018-04-28 23:40:08.623563+03	2	Диалог admin и igor	2	[{"changed": {"fields": ["delete"]}}]	18	1
184	2018-04-28 23:42:41.721201+03	2	Диалог admin и igor	2	[{"changed": {"fields": ["delete"]}}]	18	1
185	2018-04-28 23:44:53.739463+03	2	Диалог admin и igor	2	[{"changed": {"fields": ["delete_flag"]}}]	18	1
186	2018-04-28 23:45:55.489317+03	3	Диалог admin и igor	1	[{"added": {}}]	18	1
187	2018-04-28 23:47:13.629685+03	4	Диалог igor и admin	1	[{"added": {}}]	18	1
188	2018-04-28 23:48:14.973059+03	4	Диалог igor и admin	3		18	1
189	2018-04-29 00:06:56.998905+03	3	Диалог admin и igor	2	[{"added": {"name": "\\u0421\\u043e\\u043e\\u0431\\u0449\\u0435\\u043d\\u0438\\u0435", "object": "vgvghvgvhjvhvjhv jhv jgv gvgv gv gvgvjvgv j bvjgv jgvgv jgv gvjvgv jvg"}}]	18	1
190	2018-04-29 00:07:33.074934+03	3	Диалог admin и igor	2	[{"changed": {"name": "\\u0421\\u043e\\u043e\\u0431\\u0449\\u0435\\u043d\\u0438\\u0435", "object": "vgvghvgvhjvhvjhv jhv jgv gvgv gv gvgvjvgv j bvjgv jgvgv jgv gvjvgv jvg", "fields": ["profile"]}}]	18	1
191	2018-04-29 02:44:38.746111+03	3	Диалог admin и igor	2	[{"added": {"name": "\\u0421\\u043e\\u043e\\u0431\\u0449\\u0435\\u043d\\u0438\\u0435", "object": "mjjnjn"}}]	18	1
192	2018-04-29 02:45:20.485205+03	3	Диалог admin и igor	2	[{"changed": {"name": "\\u0421\\u043e\\u043e\\u0431\\u0449\\u0435\\u043d\\u0438\\u0435", "object": "mjjnjn", "fields": ["profile"]}}]	18	1
193	2018-04-29 02:45:50.823004+03	3	Диалог admin и igor	2	[{"changed": {"name": "\\u0421\\u043e\\u043e\\u0431\\u0449\\u0435\\u043d\\u0438\\u0435", "object": "vgvghvgvhjvhvjhv jhv jgv gvgv gv gvgvjvgv j bvjgv jgvgv jgv gvjvgv jvg", "fields": ["read"]}}, {"changed": {"name": "\\u0421\\u043e\\u043e\\u0431\\u0449\\u0435\\u043d\\u0438\\u0435", "object": "mjjnjn", "fields": ["read"]}}]	18	1
194	2018-04-29 19:34:19.232916+03	3	Диалог admin и igor	3		18	1
195	2018-04-29 19:34:19.424045+03	1	Диалог viktor и admin	3		18	1
196	2018-04-29 19:44:17.597171+03	5	Диалог admin и viktor	1	[{"added": {}}]	18	1
197	2018-04-29 19:44:25.196312+03	6	Диалог viktor и admin	1	[{"added": {}}]	18	1
198	2018-04-29 19:48:36.386582+03	8	sadsdasd	1	[{"added": {}}]	17	1
199	2018-04-29 19:50:01.34636+03	9	sdsad	1	[{"added": {}}]	17	1
200	2018-04-29 20:05:38.421783+03	7	Диалог admin и igor	1	[{"added": {}}]	18	1
201	2018-04-29 20:05:48.289682+03	8	Диалог igor и admin	1	[{"added": {}}]	18	1
202	2018-04-29 20:06:28.643822+03	10	тест	1	[{"added": {}}]	17	1
203	2018-04-29 20:11:12.903317+03	9	Диалог admin и igor	1	[{"added": {}}]	18	1
204	2018-04-29 20:11:20.962998+03	10	Диалог igor и admin	1	[{"added": {}}]	18	1
205	2018-04-29 20:11:25.649043+03	10	тест	2	[{"changed": {"fields": ["dialogs"]}}]	17	1
206	2018-04-29 21:16:34.157474+03	11	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur molestias similique sit sapiente et omnis ut atque tenetur corporis illum in saepe dignissimos repellendus, alias totam labore quibus	1	[{"added": {}}]	17	1
207	2018-04-29 21:30:23.444374+03	11	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium eaque nostrum qui nisi vero ullam voluptatum soluta fugit quam neque ipsam, odio aspernatur, provident in, iste. In veritatis, ips	2	[{"changed": {"fields": ["message"]}}]	17	1
208	2018-04-29 21:31:11.417181+03	12	yes	1	[{"added": {}}]	17	1
209	2018-04-29 21:31:43.343291+03	12	yes	2	[]	17	1
210	2018-04-29 21:32:04.767765+03	13	shjdhsjhdjash dshdjhas jd hsjadhjs ahdjsah djsahdjash jds	1	[{"added": {}}]	17	1
211	2018-04-29 21:34:25.092547+03	14	asdsadasd sasd sa dsadsa dsads dsa sdas asd sad sad asd sad sad asd as asdas dsads,fldmflmd lfmd fsd fdf df 5df5 1d5f 1d5f 1d51 f5d15f 1d5 1f5d1f d	1	[{"added": {}}]	17	1
212	2018-04-29 22:58:20.571424+03	14	asdsadasd sasd sa dsadsa dsads dsa sdas asd sad sad asd sad sad asd as asdas dsads,fldmflmd lfmd fsd fdf df 5df5 1d5f 1d5f 1d51 f5d15f 1d5 1f5d1f d	2	[{"changed": {"fields": ["read"]}}]	17	1
213	2018-04-29 23:10:43.249404+03	13	shjdhsjhdjash dshdjhas jd hsjadhjs ahdjsah djsahdjash jds	2	[{"changed": {"fields": ["read"]}}]	17	1
214	2018-05-01 01:27:58.85276+03	1	Публикация	1	[{"added": {}}]	19	1
215	2018-05-01 01:28:41.891891+03	2	Прием ставок	1	[{"added": {}}]	19	1
216	2018-05-01 01:30:57.771442+03	3	Утверждение условий	1	[{"added": {}}]	19	1
217	2018-05-01 01:31:14.68987+03	4	Выполнение проекта	1	[{"added": {}}]	19	1
218	2018-05-01 01:31:51.948275+03	5	Обмен отзывами	1	[{"added": {}}]	19	1
219	2018-05-01 01:33:38.719802+03	6	Завершен	1	[{"added": {}}]	19	1
220	2018-05-01 01:33:52.076002+03	7	Завершен без выполнения	1	[{"added": {}}]	19	1
221	2018-05-01 01:34:02.689222+03	8	Истек срок	1	[{"added": {}}]	19	1
222	2018-05-03 10:11:45.269882+03	15	asdknsakdn	1	[{"added": {}}]	17	1
223	2018-05-03 10:13:51.347723+03	15	asdknsakdn	2	[{"changed": {"fields": ["read"]}}]	17	1
224	2018-05-09 17:57:25.079892+03	9	Ожидает оплаты	1	[{"added": {}}]	19	1
225	2018-05-10 12:51:56.906231+03	3	Ремонт	2	[{"changed": {"fields": ["performer"]}}]	22	1
226	2018-05-10 13:27:15.458714+03	3	Ремонт	2	[{"changed": {"fields": ["status"]}}]	22	1
227	2018-05-10 14:18:42.567264+03	1	sdasdasd	1	[{"added": {}}]	21	1
228	2018-05-10 19:49:14.608017+03	3	Утверждение условий	3		19	1
229	2018-05-10 19:49:33.104975+03	1	Публикация	3		19	1
230	2018-05-10 19:50:42.083459+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["status"]}}]	22	1
231	2018-05-10 19:50:54.221569+03	7	выыва	2	[{"changed": {"fields": ["status"]}}]	22	1
232	2018-05-10 19:51:06.238081+03	6	ыыфоврыво	2	[{"changed": {"fields": ["status"]}}]	22	1
233	2018-05-10 19:51:27.17791+03	4	Ремонт	2	[{"changed": {"fields": ["status"]}}]	22	1
234	2018-05-10 19:52:26.150595+03	5	Тестовый проект	2	[{"changed": {"fields": ["status"]}}]	22	1
235	2018-05-10 19:52:38.896274+03	2	asdsad	2	[{"changed": {"fields": ["status"]}}]	22	1
236	2018-05-10 22:40:15.607485+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["description"]}}]	22	1
237	2018-05-11 10:08:18.963872+03	3	Сделаю все быстро и качественно	3		21	1
238	2018-05-11 10:36:11.444466+03	4	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi u	2	[{"changed": {"fields": ["message"]}}]	21	1
239	2018-05-11 10:42:19.121648+03	4	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi u	2	[{"changed": {"fields": ["performer"]}}]	21	1
240	2018-05-11 11:08:26.45372+03	2	asdsad	2	[{"changed": {"fields": ["status"]}}]	22	1
241	2018-05-11 11:14:33.413544+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["status"]}}]	22	1
242	2018-05-11 11:22:09.685385+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["status"]}}]	22	1
243	2018-05-11 11:22:18.652965+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["status"]}}]	22	1
244	2018-05-11 11:22:39.816108+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["status"]}}]	22	1
245	2018-05-11 11:23:05.835201+03	7	Закрыт	2	[{"changed": {"fields": ["name"]}}]	19	1
246	2018-05-11 11:23:17.959477+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["status"]}}]	22	1
247	2018-05-11 11:23:31.199031+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["status"]}}]	22	1
248	2018-05-11 11:37:56.653787+03	7	dfjdshfjsdfjdhjfhsdf	3		21	1
249	2018-05-11 11:37:56.72208+03	5	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi u	3		21	1
250	2018-05-11 11:37:56.726077+03	4	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi u	3		21	1
251	2018-05-11 11:37:56.729081+03	1	sdasdasd	3		21	1
252	2018-05-11 11:48:57.086103+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["performer"]}}]	22	1
253	2018-05-11 11:49:19.768291+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["status"]}}]	22	1
254	2018-05-11 14:02:56.983263+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["status", "performer"]}}]	22	1
255	2018-05-11 14:20:32.829758+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["status", "performer"]}}]	22	1
256	2018-05-11 14:21:42.300067+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["status", "performer"]}}]	22	1
257	2018-05-11 14:23:59.210443+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["status"]}}]	22	1
258	2018-05-11 14:24:26.53417+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["status"]}}]	22	1
259	2018-05-11 14:25:01.452265+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["status"]}}]	22	1
260	2018-05-11 14:25:14.976882+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["status"]}}]	22	1
261	2018-05-11 14:25:37.439605+03	8	asdsad	3		21	1
262	2018-05-11 14:32:10.744791+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["status"]}}]	22	1
263	2018-05-11 14:32:25.05976+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["status", "performer"]}}]	22	1
264	2018-05-11 14:32:42.262033+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["status"]}}]	22	1
265	2018-05-11 14:33:21.033481+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["performer"]}}]	22	1
266	2018-05-11 19:42:31.443421+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["status"]}}]	22	1
267	2018-05-11 19:42:42.267897+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["status"]}}]	22	1
268	2018-05-11 19:43:26.972956+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["status"]}}]	22	1
269	2018-05-11 19:43:40.490379+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["status"]}}]	22	1
270	2018-05-11 19:44:15.237648+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["status"]}}]	22	1
271	2018-05-11 20:27:09.824773+03	6	ыыфоврыво	2	[{"changed": {"fields": ["status"]}}]	22	1
272	2018-05-11 20:32:23.830499+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["status"]}}]	22	1
273	2018-05-11 21:53:07.967442+03	1	ывфывф	1	[{"added": {}}]	23	1
274	2018-05-12 12:58:38.439472+03	13	shjdhsjhdjash dshdjhas jd hsjadhjs ahdjsah djsahdjash jds	2	[{"changed": {"fields": ["profile", "project"]}}]	17	1
275	2018-05-12 12:58:49.196195+03	12	yes	2	[{"changed": {"fields": ["profile", "project"]}}]	17	1
276	2018-05-12 12:58:59.110779+03	11	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium eaque nostrum qui nisi vero ullam voluptatum soluta fugit quam neque ipsam, odio aspernatur, provident in, iste. In veritatis, ips	2	[{"changed": {"fields": ["project"]}}]	17	1
277	2018-05-12 12:59:05.15449+03	14	asdsadasd sasd sa dsadsa dsads dsa sdas asd sad sad asd sad sad asd as asdas dsads,fldmflmd lfmd fsd fdf df 5df5 1d5f 1d5f 1d51 f5d15f 1d5 1f5d1f d	2	[{"changed": {"fields": ["project"]}}]	17	1
278	2018-05-12 12:59:10.676609+03	15	asdknsakdn	2	[{"changed": {"fields": ["project"]}}]	17	1
279	2018-05-12 12:59:17.988484+03	9	sdsad	2	[{"changed": {"fields": ["project"]}}]	17	1
280	2018-05-12 12:59:25.658837+03	8	sadsdasd	2	[{"changed": {"fields": ["project"]}}]	17	1
281	2018-05-12 13:05:49.348564+03	16	bjnjnjn	1	[{"added": {}}]	17	1
282	2018-05-12 13:06:06.911051+03	16	bjnjnjn	3		17	1
283	2018-05-12 13:21:26.04017+03	8	sadsdasd	3		17	1
284	2018-05-12 13:38:35.62199+03	15	asdknsakdn	2	[{"changed": {"fields": ["read"]}}]	17	1
285	2018-05-12 14:47:18.855796+03	5	manager	1	[{"added": {}}]	4	1
286	2018-05-12 14:48:49.914015+03	8	safdfdffsdfds	2	[{"changed": {"fields": ["manager"]}}]	22	1
287	2018-05-12 14:49:51.406246+03	17	увыфвыф	1	[{"added": {}}]	17	1
288	2018-05-12 14:51:32.948987+03	5	manager	2	[{"changed": {"fields": ["first_name", "last_name"]}}]	4	1
289	2018-05-12 15:24:25.827702+03	17	увыфвыф	3		17	1
290	2018-05-12 15:24:25.858954+03	15	asdknsakdn	3		17	1
291	2018-05-12 15:24:25.858954+03	14	asdsadasd sasd sa dsadsa dsads dsa sdas asd sad sad asd sad sad asd as asdas dsads,fldmflmd lfmd fsd fdf df 5df5 1d5f 1d5f 1d51 f5d15f 1d5 1f5d1f d	3		17	1
292	2018-05-12 15:24:25.858954+03	13	shjdhsjhdjash dshdjhas jd hsjadhjs ahdjsah djsahdjash jds	3		17	1
293	2018-05-12 15:24:25.858954+03	12	yes	3		17	1
294	2018-05-12 15:24:25.858954+03	11	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium eaque nostrum qui nisi vero ullam voluptatum soluta fugit quam neque ipsam, odio aspernatur, provident in, iste. In veritatis, ips	3		17	1
295	2018-05-12 15:24:25.858954+03	9	sdsad	3		17	1
296	2018-05-12 15:49:37.969436+03	1	ывфывф	3		23	1
297	2018-05-12 15:49:55.443338+03	11	Gsasd	3		22	1
298	2018-05-12 15:49:55.469588+03	9	Новый проект	3		22	1
299	2018-05-12 15:49:55.47159+03	8	safdfdffsdfds	3		22	1
300	2018-05-12 15:49:55.473592+03	7	выыва	3		22	1
301	2018-05-12 15:49:55.475153+03	6	ыыфоврыво	3		22	1
302	2018-05-12 15:49:55.477098+03	5	Тестовый проект	3		22	1
303	2018-05-12 15:49:55.478601+03	4	Ремонт	3		22	1
304	2018-05-12 16:00:26.506446+03	12	Сантехника в доме	2	[{"changed": {"fields": ["status"]}}]	22	1
305	2018-05-12 16:10:08.92982+03	12	Сантехника в доме	2	[{"changed": {"fields": ["manager"]}}]	22	1
306	2018-05-12 16:10:35.652223+03	5	manager	2	[{"changed": {"fields": ["is_staff"]}}]	4	1
307	2018-05-12 17:20:22.406223+03	3	Лучший заказчик в мире!	3		23	1
308	2018-05-12 17:21:57.232282+03	4	Лучший заказчик в мире	3		23	1
309	2018-05-12 17:22:51.265665+03	5	Лучший заказчик	3		23	1
310	2018-05-12 17:23:52.108933+03	12	Сантехника в доме	2	[{"changed": {"fields": ["status"]}}]	22	1
311	2018-05-12 17:24:25.614815+03	6	Лучший заказчик	3		23	1
312	2018-05-12 18:52:33.685728+03	2	igor	2	[{"changed": {"fields": ["town"]}}]	8	1
313	2018-05-12 23:54:13.909024+03	6	shgadh	3		4	1
314	2018-05-13 00:01:27.641626+03	7	asdasda	3		4	1
315	2018-05-13 00:02:43.393322+03	8	asdasd	3		4	1
316	2018-05-13 00:05:26.455975+03	9	asdas	3		4	1
317	2018-05-13 00:07:41.056254+03	10	aasdas	3		4	1
318	2018-05-13 00:11:40.720892+03	11	asdasdas	3		4	1
319	2018-05-13 00:13:26.818154+03	12	sada	3		4	1
320	2018-05-13 00:15:24.475683+03	13	asdasd	3		4	1
321	2018-05-13 00:20:36.704171+03	14	asdasdas	3		4	1
322	2018-05-13 01:12:20.129768+03	15	sdada	3		4	1
323	2018-05-13 01:12:20.1758+03	16	sdada1	3		4	1
324	2018-05-13 01:16:04.552101+03	17	ASas	3		4	1
325	2018-05-13 01:24:08.257808+03	18	asdasd	3		4	1
326	2018-05-14 00:31:07.716101+03	13	Укладка плитки на кухне	2	[{"changed": {"fields": ["status"]}}]	22	1
327	2018-05-14 00:40:11.182459+03	14	Укладка паркета в гостинной	2	[{"changed": {"fields": ["status"]}}]	22	1
328	2018-05-14 00:40:50.285239+03	14	Укладка паркета в гостинной	2	[{"changed": {"fields": ["description"]}}]	22	1
329	2018-05-14 03:44:57.072668+03	14	Укладка паркета в гостинной	2	[{"changed": {"fields": ["customer"]}}]	22	1
330	2018-05-14 03:45:17.497669+03	14	Укладка паркета в гостинной	2	[{"changed": {"fields": ["customer"]}}]	22	1
331	2018-05-17 12:08:15.491321+03	15	Проект	2	[{"changed": {"fields": ["status"]}}]	22	1
332	2018-05-27 22:30:16.580112+03	14	Укладка паркета в гостинной	2	[{"changed": {"fields": ["status"]}}]	22	1
333	2018-05-27 22:31:09.969335+03	15	Проект	2	[{"changed": {"fields": ["status"]}}]	22	1
334	2018-05-27 22:43:35.35534+03	16	ывыфвл	2	[{"changed": {"fields": ["status"]}}]	22	1
\.


--
-- TOC entry 3137 (class 0 OID 24580)
-- Dependencies: 197
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2018-01-04 17:45:04.120037+03
2	auth	0001_initial	2018-01-04 17:45:05.018474+03
3	admin	0001_initial	2018-01-04 17:45:05.250092+03
4	admin	0002_logentry_remove_auto_add	2018-01-04 17:45:05.267609+03
5	contenttypes	0002_remove_content_type_name	2018-01-04 17:45:05.312643+03
6	auth	0002_alter_permission_name_max_length	2018-01-04 17:45:05.346666+03
7	auth	0003_alter_user_email_max_length	2018-01-04 17:45:05.374189+03
8	auth	0004_alter_user_username_opts	2018-01-04 17:45:05.391705+03
9	auth	0005_alter_user_last_login_null	2018-01-04 17:45:05.41372+03
10	auth	0006_require_contenttypes_0002	2018-01-04 17:45:05.417723+03
11	auth	0007_alter_validators_add_error_messages	2018-01-04 17:45:05.432732+03
12	auth	0008_alter_user_username_max_length	2018-01-04 17:45:05.491054+03
13	sessions	0001_initial	2018-01-04 17:45:05.635451+03
14	estimate	0001_initial	2018-01-04 22:51:56.519065+03
15	estimate	0002_auto_20180331_1902	2018-03-31 19:03:33.941508+03
16	userProfile	0001_initial	2018-03-31 19:03:35.555673+03
17	userProfile	0002_auto_20180331_1915	2018-03-31 19:16:03.848871+03
18	userProfile	0003_auto_20180331_1943	2018-03-31 19:44:09.452379+03
19	userProfile	0004_auto_20180331_2001	2018-03-31 20:01:22.578474+03
20	userProfile	0005_auto_20180331_2005	2018-03-31 20:05:58.974355+03
21	userProfile	0006_userprofile_summary	2018-03-31 20:10:38.37719+03
22	userProfile	0007_userprofile_skype	2018-03-31 23:55:06.784481+03
23	userProfile	0008_auto_20180401_0109	2018-04-01 01:09:40.341863+03
24	userProfile	0009_auto_20180401_2047	2018-04-01 20:47:56.231695+03
25	portfolio	0001_initial	2018-04-08 20:41:38.714325+03
26	portfolio	0002_auto_20180408_2046	2018-04-08 20:47:21.59671+03
27	portfolio	0003_auto_20180408_2049	2018-04-08 20:49:40.392394+03
28	portfolio	0004_fileportfolio	2018-04-08 21:05:05.352873+03
29	portfolio	0005_fileportfolio_name	2018-04-08 21:07:13.263374+03
30	portfolio	0006_auto_20180408_2112	2018-04-08 21:12:26.427216+03
31	portfolio	0007_auto_20180408_2112	2018-04-08 21:12:48.995722+03
32	portfolio	0008_auto_20180408_2113	2018-04-08 21:13:51.033732+03
33	portfolio	0009_auto_20180408_2125	2018-04-08 21:25:25.841639+03
34	userProfile	0010_auto_20180408_2203	2018-04-08 22:33:24.714921+03
35	userProfile	0011_auto_20180408_2233	2018-04-08 22:33:24.922339+03
36	userProfile	0012_userprofile_specializations	2018-04-08 22:36:38.447833+03
37	userProfile	0013_auto_20180408_2237	2018-04-08 22:38:00.305552+03
38	portfolio	0010_auto_20180417_2012	2018-04-17 20:12:25.042123+03
39	portfolio	0011_auto_20180417_2336	2018-04-17 23:36:36.263336+03
40	portfolio	0012_auto_20180419_0052	2018-04-19 00:52:30.683194+03
41	portfolio	0013_auto_20180419_0055	2018-04-19 00:55:57.18137+03
42	userProfile	0014_auto_20180426_2314	2018-04-26 23:14:32.627379+03
43	userProfile	0015_auto_20180426_2318	2018-04-26 23:18:50.131405+03
44	auth	0009_alter_user_last_name_max_length	2018-04-27 19:18:38.788455+03
45	userProfile	0016_auto_20180427_1905	2018-04-27 19:18:39.29112+03
46	serviceRepair.messages	0001_initial	2018-04-27 22:56:17.840032+03
47	serviceRepair.messages	0002_auto_20180427_2300	2018-04-27 23:00:06.002435+03
48	Users_messages	0001_initial	2018-04-28 00:01:14.33725+03
49	Users_messages	0002_auto_20180427_2300	2018-04-28 00:01:14.559899+03
50	Users_messages	0003_auto_20180428_0001	2018-04-28 00:01:15.408731+03
51	userProfile	0017_userprofile_specialization	2018-04-28 19:00:53.404316+03
52	Users_messages	0004_dialogs_delete	2018-04-28 22:44:02.047789+03
53	userProfile	0018_auto_20180428_2243	2018-04-28 22:44:02.113833+03
54	Users_messages	0005_auto_20180428_2343	2018-04-28 23:43:50.381665+03
55	Users_messages	0006_auto_20180429_1941	2018-04-29 19:42:06.429124+03
56	Users_messages	0007_remove_dialogs_delete_flag	2018-04-29 19:42:06.450137+03
57	portfolio	0014_auto_20180430_1928	2018-04-30 19:28:13.707598+03
58	projects	0001_initial	2018-04-30 23:56:07.509565+03
59	projects	0002_auto_20180501_0004	2018-05-01 00:04:33.907093+03
60	projects	0003_auto_20180501_0018	2018-05-01 00:35:17.633872+03
61	projects	0004_auto_20180501_0035	2018-05-01 00:35:17.663218+03
62	projects	0005_auto_20180501_0109	2018-05-01 01:09:17.624889+03
63	projects	0006_auto_20180501_0121	2018-05-01 01:22:05.86221+03
64	projects	0007_review_read	2018-05-01 01:44:58.634195+03
65	projects	0008_auto_20180509_1813	2018-05-09 18:13:57.022554+03
66	projects	0009_auto_20180509_1823	2018-05-09 18:23:09.948278+03
67	projects	0010_auto_20180510_1040	2018-05-10 10:40:10.427918+03
68	projects	0011_auto_20180510_1059	2018-05-10 10:59:51.570054+03
69	projects	0012_auto_20180510_1119	2018-05-10 11:20:04.865868+03
70	projects	0013_auto_20180510_1211	2018-05-10 12:11:46.568435+03
71	projects	0014_auto_20180511_1009	2018-05-11 10:09:37.067829+03
72	projects	0015_rate_date_time	2018-05-11 11:38:47.835736+03
73	projects	0016_auto_20180511_1154	2018-05-11 11:55:01.235824+03
74	projects	0017_auto_20180511_1540	2018-05-11 15:40:38.667779+03
75	projects	0018_auto_20180511_2039	2018-05-11 20:39:23.857252+03
76	projects	0019_review_date_time	2018-05-11 22:32:07.201946+03
77	projects	0020_project_manager	2018-05-12 11:40:10.059059+03
78	Users_messages	0008_auto_20180512_1143	2018-05-12 11:43:42.839457+03
79	Users_messages	0009_auto_20180512_1501	2018-05-12 15:01:23.477431+03
80	userProfile	0019_userprofile_info	2018-05-12 23:08:01.78256+03
81	userProfile	0020_auto_20180512_2313	2018-05-12 23:13:51.650292+03
\.


--
-- TOC entry 3154 (class 0 OID 24733)
-- Dependencies: 214
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
1fwtz0fjtug63h18japt452v2a9f2prn	ZTY3OWQ0NGIwYjU4NmVmYzUxMGMwMjNhNDExMzhmYTc4MzdlMjY4Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0YTRiMjBkYTIyNjg1Yzk1OGYzZDFmYTdhNTJiNzU5MjRlNDlhMDBhIn0=	2018-01-18 17:49:02.241797+03
hokrnhhwaeovbjmyf8mtgibq9yklh7dj	ZTY3OWQ0NGIwYjU4NmVmYzUxMGMwMjNhNDExMzhmYTc4MzdlMjY4Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0YTRiMjBkYTIyNjg1Yzk1OGYzZDFmYTdhNTJiNzU5MjRlNDlhMDBhIn0=	2018-03-29 13:40:02.633892+03
tvr1pe79mlraqay8qauw75a1ptuzhixc	ODgyOGQ3NjQ5YTUyZWEzNzIyY2YzZWRhN2M4Mjk0NjI2YjRlNWM1Njp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIzZTM3ZGUzYzBkZGQ5OTkzZDIwNGM3ZWNmY2E0ODdhNjFkNjMxZGI3In0=	2018-05-30 10:38:15.055556+03
p6qsszk9hu2ivrf21jrcihc7vqg39xga	ZTY3OWQ0NGIwYjU4NmVmYzUxMGMwMjNhNDExMzhmYTc4MzdlMjY4Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0YTRiMjBkYTIyNjg1Yzk1OGYzZDFmYTdhNTJiNzU5MjRlNDlhMDBhIn0=	2018-04-22 20:42:01.967294+03
qacdlghwif9h5zsqx0w1p5lh5nhr88yz	ZTY3OWQ0NGIwYjU4NmVmYzUxMGMwMjNhNDExMzhmYTc4MzdlMjY4Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0YTRiMjBkYTIyNjg1Yzk1OGYzZDFmYTdhNTJiNzU5MjRlNDlhMDBhIn0=	2018-04-12 01:36:10.983442+03
zrcyi914t8qqnqmr099qdjlemh4l9qf7	ODgyOGQ3NjQ5YTUyZWEzNzIyY2YzZWRhN2M4Mjk0NjI2YjRlNWM1Njp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIzZTM3ZGUzYzBkZGQ5OTkzZDIwNGM3ZWNmY2E0ODdhNjFkNjMxZGI3In0=	2018-06-11 00:00:39.315134+03
zdrwp42y44veoxnb7aycoicls3oehyfs	NmY1YzU5NThiMmFmNjJiYzhjOTgwMDlhNWI4M2YyNjFiMDRmMjNlZjp7fQ==	2018-04-14 19:04:22.827361+03
kf3syzomk4tge903d38sfy5pjcxcpq69	NmY1YzU5NThiMmFmNjJiYzhjOTgwMDlhNWI4M2YyNjFiMDRmMjNlZjp7fQ==	2018-04-14 19:05:10.679545+03
\.


--
-- TOC entry 3156 (class 0 OID 24745)
-- Dependencies: 216
-- Data for Name: estimate_typedesign; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY estimate_typedesign (id, name, image) FROM stdin;
2	Огни большого города	design/design1.jpg
3	Эта замечательная жизнь	design/design2.jpg
4	В погоне за счастьем	design/design3.jpg
5	Пробуждение	design/design4.jpg
\.


--
-- TOC entry 3166 (class 0 OID 32953)
-- Dependencies: 226
-- Data for Name: portfolio_portfolio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY portfolio_portfolio (id, name, description, profile_id, image) FROM stdin;
14	sda1	Lorem ipsum dolor sit amet, consectetur adipisicing elit. In, numquam nostrum ullam! Unde, temporibus atque aperiam enim accusantium repudiandae vel ratione qui consectetur hic iste ab labore fugiat dolores deleniti veniam facilis, possimus in eius odio modi minus. Provident at reiciendis laborum non magnam deleniti aliquam cupiditate rem facilis dolor quas officia voluptate iste ratione temporibus accusamus repudiandae omnis, hic assumenda. At esse id molestiae, tempore eos cum et quod praesentium, beatae, dolor hic est tempora architecto placeat similique. Voluptas quaerat nostrum cum molestias mollitia, voluptatem animi, esse voluptates nam nesciunt iusto molestiae ratione veritatis illo vel sapiente cumque quidem obcaecati dolore alias iste. Repellendus natus asperiores voluptatum sit, sed ipsam dolorem! Adipisci unde quae vitae explicabo sed deserunt ipsum earum maxime placeat! Aspernatur optio, quos esse odio eius incidunt excepturi corrupti recusandae laborum corporis, quis ut quo mollitia ab saepe dicta provident autem numquam cum deleniti. Provident veritatis commodi assumenda, ut possimus laudantium voluptas facere eaque quod sequi magnam maiores. Odit officiis voluptate esse repudiandae deleniti quibusdam, recusandae sequi libero quae molestiae maxime culpa, cum atque sed magnam cupiditate distinctio vero deserunt iure. Recusandae quos placeat possimus, aliquam, ipsa magni inventore vel doloremque necessitatibus perferendis deserunt adipisci. Reiciendis, odio!	1	users/1/portfolio_14/images/anondog.png
13	xdd	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.	1	users/1/portfolio_13/images/Qem2-vKSRVw.jpg
\.


--
-- TOC entry 3168 (class 0 OID 32978)
-- Dependencies: 228
-- Data for Name: portfolio_fileportfolio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY portfolio_fileportfolio (id, file, portfolio_id, name) FROM stdin;
4	users/1/portfolio_14/files/Лист_Microsoft_Excel.xlsx	14	Файл проектного решения
6	users/1/portfolio_14/files/Rekomendovanny_poryadok_chtenia_nashikh_komixov.pdf	14	Смета
\.


--
-- TOC entry 3164 (class 0 OID 32945)
-- Dependencies: 224
-- Data for Name: portfolio_imagesportfolio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY portfolio_imagesportfolio (id, image, portfolio_id) FROM stdin;
5	users/1/portfolio_14/images/Bk7XzHcKbcE.jpg	14
8	users/1/portfolio_14/images/Qem2-vKSRVw.jpg	14
9	users/1/portfolio_14/images/20170308_154447.jpg	14
\.


--
-- TOC entry 3180 (class 0 OID 33260)
-- Dependencies: 240
-- Data for Name: projects_fileproject; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY projects_fileproject (id, name, file, project_id) FROM stdin;
\.


--
-- TOC entry 3186 (class 0 OID 33285)
-- Dependencies: 246
-- Data for Name: projects_rate; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY projects_rate (id, message, term, money, performer_id, project_id, date_time) FROM stdin;
12	Готов выполнить проект в кратчайшие сроки	9	12000.00	2	12	2018-05-12 16:01:16.89957+03
13	Готов выполнить проект	11	13000.00	1	12	2018-05-12 16:02:36.775617+03
14	Готов выполнить проект	5	4000.00	1	13	2018-05-16 10:28:35.397573+03
15	vgsdvhsadhsavd	10	5000.00	1	15	2018-05-17 12:09:03.359034+03
16	ммпм	6	6000.00	1	16	2018-05-27 22:44:25.269058+03
\.


--
-- TOC entry 3188 (class 0 OID 33372)
-- Dependencies: 248
-- Data for Name: projects_review; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY projects_review (id, message, profile_from_id, profile_to_id, project_id, evaluation, date_time) FROM stdin;
2	Все отлично	3	2	12	4	2018-05-12 17:13:59.544921+03
7	vgvg	2	3	12	5	2018-05-12 17:24:44.478828+03
8	Отличный исполнитель. Все сделано качественно а главное в строк	3	1	13	3	2018-05-16 22:36:45.962215+03
9	bjbdsjabd	1	3	15	4	2018-05-17 12:12:50.528464+03
\.


--
-- TOC entry 3176 (class 0 OID 33104)
-- Dependencies: 236
-- Data for Name: serviceRepair.messages_messages; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "serviceRepair.messages_messages" (id, message, date_time, profile_from_id, profile_to_id, read) FROM stdin;
\.


--
-- TOC entry 3174 (class 0 OID 33042)
-- Dependencies: 234
-- Data for Name: userProfile_userprofile_specializations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "userProfile_userprofile_specializations" (id, userprofile_id, specialization_id) FROM stdin;
25	1	24
26	1	27
27	1	30
28	1	31
\.


--
-- TOC entry 3193 (class 0 OID 0)
-- Dependencies: 237
-- Name: Users_messages_messages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"Users_messages_messages_id_seq"', 29, true);


--
-- TOC entry 3194 (class 0 OID 0)
-- Dependencies: 202
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_group_id_seq', 2, true);


--
-- TOC entry 3195 (class 0 OID 0)
-- Dependencies: 204
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 6, true);


--
-- TOC entry 3196 (class 0 OID 0)
-- Dependencies: 200
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_permission_id_seq', 69, true);


--
-- TOC entry 3197 (class 0 OID 0)
-- Dependencies: 208
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 11, true);


--
-- TOC entry 3198 (class 0 OID 0)
-- Dependencies: 206
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_id_seq', 19, true);


--
-- TOC entry 3199 (class 0 OID 0)
-- Dependencies: 210
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- TOC entry 3200 (class 0 OID 0)
-- Dependencies: 212
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 334, true);


--
-- TOC entry 3201 (class 0 OID 0)
-- Dependencies: 198
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_content_type_id_seq', 23, true);


--
-- TOC entry 3202 (class 0 OID 0)
-- Dependencies: 196
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_migrations_id_seq', 81, true);


--
-- TOC entry 3203 (class 0 OID 0)
-- Dependencies: 215
-- Name: estimate_typedesign_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('estimate_typedesign_id_seq', 5, true);


--
-- TOC entry 3204 (class 0 OID 0)
-- Dependencies: 227
-- Name: portfolio_fileportfolio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('portfolio_fileportfolio_id_seq', 6, true);


--
-- TOC entry 3205 (class 0 OID 0)
-- Dependencies: 223
-- Name: portfolio_imagesportfolio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('portfolio_imagesportfolio_id_seq', 9, true);


--
-- TOC entry 3206 (class 0 OID 0)
-- Dependencies: 225
-- Name: portfolio_portfolio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('portfolio_portfolio_id_seq', 15, true);


--
-- TOC entry 3207 (class 0 OID 0)
-- Dependencies: 239
-- Name: projects_fileproject_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('projects_fileproject_id_seq', 3, true);


--
-- TOC entry 3208 (class 0 OID 0)
-- Dependencies: 241
-- Name: projects_project_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('projects_project_id_seq', 16, true);


--
-- TOC entry 3209 (class 0 OID 0)
-- Dependencies: 243
-- Name: projects_projectstatus_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('projects_projectstatus_id_seq', 9, true);


--
-- TOC entry 3210 (class 0 OID 0)
-- Dependencies: 245
-- Name: projects_rate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('projects_rate_id_seq', 16, true);


--
-- TOC entry 3211 (class 0 OID 0)
-- Dependencies: 247
-- Name: projects_review_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('projects_review_id_seq', 9, true);


--
-- TOC entry 3212 (class 0 OID 0)
-- Dependencies: 235
-- Name: serviceRepair.messages_messages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"serviceRepair.messages_messages_id_seq"', 3, true);


--
-- TOC entry 3213 (class 0 OID 0)
-- Dependencies: 229
-- Name: userProfile_specialization_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"userProfile_specialization_id_seq"', 38, true);


--
-- TOC entry 3214 (class 0 OID 0)
-- Dependencies: 231
-- Name: userProfile_specializationgroup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"userProfile_specializationgroup_id_seq"', 10, true);


--
-- TOC entry 3215 (class 0 OID 0)
-- Dependencies: 221
-- Name: userProfile_statuses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"userProfile_statuses_id_seq"', 5, true);


--
-- TOC entry 3216 (class 0 OID 0)
-- Dependencies: 219
-- Name: userProfile_towns_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"userProfile_towns_id_seq"', 2, true);


--
-- TOC entry 3217 (class 0 OID 0)
-- Dependencies: 217
-- Name: userProfile_userprofile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"userProfile_userprofile_id_seq"', 20, true);


--
-- TOC entry 3218 (class 0 OID 0)
-- Dependencies: 233
-- Name: userProfile_userprofile_specializations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"userProfile_userprofile_specializations_id_seq"', 28, true);


-- Completed on 2018-05-30 11:10:00

--
-- PostgreSQL database dump complete
--

