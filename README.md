# Сайт услуг по ремонту квартир
- - -

## Развертывание БД

1. PostgresSql ставить [отсюда](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads) версия 10.3 ([это видео](https://www.youtube.com/watch?v=e1MwsT5FJRQ) может помочь)

2. После этого нужно будет зайти в servicerepair->serviceRepair->serviceRepair->settings.py и найти раздел "DATABASES" и изменить параметры сервера и БД на свои:

```python
DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.postgresql_psycopg2',
		'NAME': 'ServiceRepair',
		'USER': 'postgres',
		'PASSWORD': '12345678',
		'HOST': 'localhost',
		'PORT': '5432'
	}
}
```
## Развертывание проекта

1. Устанавливаем виртуальное окружение [отсюда](https://virtualenv.pypa.io/en/stable/installation/)
([это видео](https://www.youtube.com/watch?v=3G7Tb7xvsy0&t=0s&list=PLficgFmiYCC9EKusSZd4xcALRnKXe9QaM&index=4) может помочь)

2. Открываем командную строку, переходим в папку с проектом и пишем "virtualenv ENV" (без кавычек) для создания виртуального окружения проекта

3. После создания в командной строке пишем "ENV\Scripts\activate" (без кавычек) для активации виртуального окружения

4. Далее в командной строке вводим "pip install -r requirements.txt" (без кавычек) для установки всех необходимых пакетов

## Запуск проекта

1. Открываем командную строку и заходим в папку с проектом servicerepair->serviceRepair->serviceRepair

2. Активируем виртуальное окружение (см. выше)

3. Вводим python manage.py runserver для запуска сервера

4. Открываем любой браузер и вводим http://127.0.0.1:8000/ или http://localhost:8000

- - -

### Уроки по Django

Документация - https://djbook.ru/rel1.9/index.html

Видео уроки - https://www.youtube.com/watch?v=4RQ2USIaINs&list=PLSWnD6rL-m9adebgpvvOLH5ASGJiznWdg

Видео уроки - https://www.youtube.com/playlist?list=PLficgFmiYCC9EKusSZd4xcALRnKXe9QaM&disable_polymer=true

Документацяи по авторизации - https://djbook.ru/rel1.9/topics/auth/index.html

### Шпаргалка по git

Статья - https://proglib.io/p/git-for-half-an-hour/

Книга - https://git-scm.com/book/ru/v2